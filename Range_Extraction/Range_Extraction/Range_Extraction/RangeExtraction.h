//
//  RangeExtraction.h
//  Range_Extraction
//
//  Created by Mathew Strydom on 2019/08/22.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#ifndef RangeExtraction_h
#define RangeExtraction_h

#include <stdio.h>
#include <iostream>
#include <cmath>

//Gaussian blue and sobel edge detection filters. The gaussian blur was initially used in an attempt to counteract the effects of Jpeg compression. currently these filters are used for the harris corner detector which i reently implemeneted.

void Filter(cv::Mat originalImage , cv::Mat &Filtered, cv::Mat &Direction,  std::string type, std::string Colour){
    cv::Mat Gaussian(3,3, cv::DataType<double>::type), SobelX(3,3, cv::DataType<double>::type) , SobelY(3,3, cv::DataType<double>::type);
    //initial factor
    double factor = 1.0;
    //if a Gaussian blur is desired the input image should be either BGR or Grayscale and must be specified.
    if (type == "Blur") {
        //Gaussian kernel with factor
        Gaussian.at<double>(0,0) = 1;
        Gaussian.at<double>(0,1) = 2;
        Gaussian.at<double>(0,2) = 1;
        Gaussian.at<double>(1,0) = 2;
        Gaussian.at<double>(1,1) = 4;
        Gaussian.at<double>(1,2) = 2;
        Gaussian.at<double>(2,0) = 1;
        Gaussian.at<double>(2,1) = 2;
        Gaussian.at<double>(2,2) = 1;
        factor = 1.0/16.0;
        
        //Blurs a gray image
        if(Colour == "Gray"){
            
            for(int x = 0; x < originalImage.cols; x++){
                for(int y = 0; y < originalImage.rows; y++){
                    double grayValue = 0.0;
                    
                    // looping through the image and colvolving with the kernel. The egdes of the images are wrapped around rather than truncated
                    for(int filterY = 0; filterY < 3; filterY++){
                        for(int filterX = 0; filterX < 3; filterX++){
                            int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                            int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                            grayValue += originalImage.at<double>(imageY, imageX)*Gaussian.at<double>(filterY,filterX);
                        }
                    }
                    // The resulting intensity is not cappped
                    Filtered.at<double>(y, x) = factor*grayValue;
                }
            }
        }
        else if(Colour == "BGR"){
            for(int x = 0; x < originalImage.cols; x++){
                for(int y = 0; y < originalImage.rows; y++){
                    double blueVal = 0.0, greenVal = 0.0, redVal = 0.0;
                    
                    // looping through the image and colvolving with the kernel. The egdes of the images are wrapped around rather than truncated
                    for(int filterY = 0; filterY < 3; filterY++){
                        for(int filterX = 0; filterX < 3; filterX++){
                            int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                            int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                            blueVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[0]*Gaussian.at<double>(filterY,filterX);
                            greenVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[1]*Gaussian.at<double>(filterY,filterX);
                            redVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[2]*Gaussian.at<double>(filterY,filterX);
                        }
                    }
                    // The resulting intensities is capped between 0 and 255 for display purposes.
                    Filtered.at<cv::Vec3b>(y, x)[0] = (uchar)fmin(fmax(int(factor*blueVal), 0), 255);
                    Filtered.at<cv::Vec3b>(y, x)[1] = (uchar)fmin(fmax(int(factor*greenVal), 0), 255);
                    Filtered.at<cv::Vec3b>(y, x)[2] = (uchar)fmin(fmax(int(factor*redVal), 0), 255);
                }
            }
        }
    }
    // Sobel edge detector.
    else if(type == "Sobel"){
        // Kernel for derivative with respect to X
        SobelX.at<double>(0,0) = -1;
        SobelX.at<double>(0,1) = 0;
        SobelX.at<double>(0,2) = 1;
        SobelX.at<double>(1,0) = -2;
        SobelX.at<double>(1,1) = 0;
        SobelX.at<double>(1,2) = 2;
        SobelX.at<double>(2,0) = -1;
        SobelX.at<double>(2,1) = 0;
        SobelX.at<double>(2,2) = 1;
        
        // Kernel for derivative with respect to Y
        SobelY.at<double>(0,0) = -1;
        SobelY.at<double>(0,1) = -2;
        SobelY.at<double>(0,2) = -1;
        SobelY.at<double>(1,0) = 0;
        SobelY.at<double>(1,1) = 0;
        SobelY.at<double>(1,2) = 0;
        SobelY.at<double>(2,0) = 1;
        SobelY.at<double>(2,1) = 2;
        SobelY.at<double>(2,2) = 1;
        
        for(int x = 0; x < originalImage.cols; x++){
            for(int y = 0; y < originalImage.rows; y++){
                double Gx = 0.0, Gy = 0.0;
                // looping through the image and colvolving with the kernels. The egdes of the images are wrapped around rather than truncated
                for(int filterY = 0; filterY < 3; filterY++){
                    for(int filterX = 0; filterX < 3; filterX++){
                        int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                        int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                        Gx += (double)originalImage.at<uchar>(imageY, imageX)*SobelX.at<double>(filterY,filterX);
                        Gy += (double)originalImage.at<uchar>(imageY, imageX)*SobelY.at<double>(filterY,filterX);
                    }
                }
                
                //X derivaitve of the image
                Filtered.at<double>(y, x) = pow(Gx, 2);
                //Y derivaitve of the image
                Direction.at<double>(y, x) = pow(Gy, 2);
            }
        }
        cv::imwrite("../Dx.png", Filtered);
        cv::imwrite("../Dy.png", Direction);
    }
    
    
    return;
    
}

//Read the file created by the calibration program with all the calculated information
//1 refers to the left camera, 2 refers to the right camera, K1,2 is the intrinsic calibration matrix, R1,2 is the rotation matrix of each camera, T1,2 is the translation of the two cameras, R and T is the realtive rotation and translation of the right camera with respect to the left, C1,2 is the camera centers in the world plane, P1,2 is the projection matrices of the cameras before rectification.
void ReadFiles(cv::Mat &K1, cv::Mat &K2, cv::Mat &R1, cv::Mat &R2, cv::Mat &T1, cv::Mat &T2, cv::Mat &R, cv::Mat &T, cv::Mat &C1, cv::Mat &C2, cv::Mat &Po1, cv::Mat &Po2, int &board_width, int &board_height, double &square_size, int &num_imgs, int &cols, int &rows){
    
    cv::FileStorage fs1("../Camera_files/cam_stereo.yml", cv::FileStorage::READ);
    
    fs1["K1"] >> K1;
    fs1["K2"] >> K2;
    fs1["R1"] >> R1;
    fs1["R2"] >> R2;
    fs1["T1"] >> T1;
    fs1["T2"] >> T2;
    fs1["R"] >> R;
    fs1["T"] >> T;
    fs1["C1"] >> C1;
    fs1["C2"] >> C2;
    fs1["Po1"] >> Po1;
    fs1["Po2"] >> Po2;
    fs1["board_width"] >> board_width;
    fs1["board_height"] >> board_height;
    fs1["square_size"] >> square_size;
    fs1["num_imgs"] >> num_imgs;
    fs1["cols"] >> cols;
    fs1["rows"] >> rows;
    return;
}

// Calculate the new projection matrices for each camera which projects the images onto the same virtual plane.
void RectifyingTransform(cv::Mat Po1, cv::Mat Po2, cv::Mat R1, cv::Mat R2, cv::Mat RelR, cv::Mat RelT, cv::Mat C1, cv::Mat C2,
                         cv::Mat K1, cv::Mat K2, cv::Mat &Transform1, cv::Mat &Transform2){
    cv::Mat Qo1(3,3, cv::DataType<double>::type), Qo2(3,3, cv::DataType<double>::type), Rprime(3,3, cv::DataType<double>::type),
    Ex1(3,4, cv::DataType<double>::type), Ex2(3,4, cv::DataType<double>::type), Pn1(3,4, cv::DataType<double>::type),
    Pn2(3,4, cv::DataType<double>::type), Qn1(3,3, cv::DataType<double>::type), Qn2(3,3, cv::DataType<double>::type),Rc1(3,1, cv::DataType<double>::type), Rc2(3,1, cv::DataType<double>::type),
    rpx, rpy, rpz, Kprime;
    
    //Qo1 are the first three columns of the old projection matrix of the left camera
    Qo1.at<double>(0,0) = Po1.at<double>(0,0);
    Qo1.at<double>(0,1) = Po1.at<double>(0,1);
    Qo1.at<double>(0,2) = Po1.at<double>(0,2);
    Qo1.at<double>(1,0) = Po1.at<double>(1,0);
    Qo1.at<double>(1,1) = Po1.at<double>(1,1);
    Qo1.at<double>(1,2) = Po1.at<double>(1,2);
    Qo1.at<double>(2,0) = Po1.at<double>(2,0);
    Qo1.at<double>(2,1) = Po1.at<double>(2,1);
    Qo1.at<double>(2,2) = Po1.at<double>(2,2);
    
    //Qo2 are the first three columns of the old projection matrix of the right camera
    Qo2.at<double>(0,0) = Po2.at<double>(0,0);
    Qo2.at<double>(0,1) = Po2.at<double>(0,1);
    Qo2.at<double>(0,2) = Po2.at<double>(0,2);
    Qo2.at<double>(1,0) = Po2.at<double>(1,0);
    Qo2.at<double>(1,1) = Po2.at<double>(1,1);
    Qo2.at<double>(1,2) = Po2.at<double>(1,2);
    Qo2.at<double>(2,0) = Po2.at<double>(2,0);
    Qo2.at<double>(2,1) = Po2.at<double>(2,1);
    Qo2.at<double>(2,2) = Po2.at<double>(2,2);
    
    //rpx,rpy,rpz are the coloumn vectors of the new camera roation matrix for the new projection
    rpx = (C1-C2);
    
    //rpy is perpendicular to the new x rotation and the z rotation of the camera 1 rotation
    rpy = (R1.row(2)).t().cross(rpx);
    
    //rpz is perpendicular to the new x and y rotation axis
    rpz = (rpx).cross(rpy);
    
    //Nomalising the new rotation matrix columns
    rpx = rpx.t()/(norm(rpx));
    rpy = rpy.t()/(norm(rpy));
    rpz = rpz.t()/(norm(rpz));
    
    //Combining the above column vectors into the new rotation matrix.
    Rprime.at<double>(0,0) = rpx.at<double>(0);
    Rprime.at<double>(0,1) = rpx.at<double>(1);
    Rprime.at<double>(0,2) = rpx.at<double>(2);
    Rprime.at<double>(1,0) = rpy.at<double>(0);
    Rprime.at<double>(1,1) = rpy.at<double>(1);
    Rprime.at<double>(1,2) = rpy.at<double>(2);
    Rprime.at<double>(2,0) = rpz.at<double>(0);
    Rprime.at<double>(2,1) = rpz.at<double>(1);
    Rprime.at<double>(2,2) = rpz.at<double>(2);
    
    // Rc1 and Rc2 are the new translation vectors of the cameras
    Rc1 = -Rprime*C1;
    Rc2 = -Rprime*C2;

    // New extrinsic matrices of the two cameras (Ex1 = (R|t))
    Ex1.at<double>(0,0) = Rprime.at<double>(0,0);
    Ex1.at<double>(0,1) = Rprime.at<double>(0,1);
    Ex1.at<double>(0,2) = Rprime.at<double>(0,2);
    Ex1.at<double>(0,3) = Rc1.at<double>(0);
    Ex1.at<double>(1,0) = Rprime.at<double>(1,0);
    Ex1.at<double>(1,1) = Rprime.at<double>(1,1);
    Ex1.at<double>(1,2) = Rprime.at<double>(1,2);
    Ex1.at<double>(1,3) = Rc1.at<double>(1);
    Ex1.at<double>(2,0) = Rprime.at<double>(2,0);
    Ex1.at<double>(2,1) = Rprime.at<double>(2,1);
    Ex1.at<double>(2,2) = Rprime.at<double>(2,2);
    Ex1.at<double>(2,3) = Rc1.at<double>(2);
    
    Ex2.at<double>(0,0) = Rprime.at<double>(0,0);
    Ex2.at<double>(0,1) = Rprime.at<double>(0,1);
    Ex2.at<double>(0,2) = Rprime.at<double>(0,2);
    Ex2.at<double>(0,3) = Rc2.at<double>(0);
    Ex2.at<double>(1,0) = Rprime.at<double>(1,0);
    Ex2.at<double>(1,1) = Rprime.at<double>(1,1);
    Ex2.at<double>(1,2) = Rprime.at<double>(1,2);
    Ex2.at<double>(1,3) = Rc2.at<double>(1);
    Ex2.at<double>(2,0) = Rprime.at<double>(2,0);
    Ex2.at<double>(2,1) = Rprime.at<double>(2,1);
    Ex2.at<double>(2,2) = Rprime.at<double>(2,2);
    Ex2.at<double>(2,3) = Rc2.at<double>(2);
    
    // The new projection requires the cameras to have the same intirnsic parameters and therefore the intrinsic matrices are averaged and the skew set to 0 for further calculations
    Kprime = (K1 + K2)/2;
    Kprime.at<double>(0,1) = 0;
    
    // The new projection matrices for each camera is the intrinsic multiplied by the extrinsic matrix
    Pn1 = Kprime*Ex1;
    Pn2 = Kprime*Ex2;
    
    //Qn1 are the first three columns of the new projection matrix of the left camera
    Qn1.at<double>(0,0) = Pn1.at<double>(0,0);
    Qn1.at<double>(0,1) = Pn1.at<double>(0,1);
    Qn1.at<double>(0,2) = Pn1.at<double>(0,2);
    Qn1.at<double>(1,0) = Pn1.at<double>(1,0);
    Qn1.at<double>(1,1) = Pn1.at<double>(1,1);
    Qn1.at<double>(1,2) = Pn1.at<double>(1,2);
    Qn1.at<double>(2,0) = Pn1.at<double>(2,0);
    Qn1.at<double>(2,1) = Pn1.at<double>(2,1);
    Qn1.at<double>(2,2) = Pn1.at<double>(2,2);
    
    //Qn2 are the first three columns of the new projection matrix of the right camera
    Qn2.at<double>(0,0) = Pn2.at<double>(0,0);
    Qn2.at<double>(0,1) = Pn2.at<double>(0,1);
    Qn2.at<double>(0,2) = Pn2.at<double>(0,2);
    Qn2.at<double>(1,0) = Pn2.at<double>(1,0);
    Qn2.at<double>(1,1) = Pn2.at<double>(1,1);
    Qn2.at<double>(1,2) = Pn2.at<double>(1,2);
    Qn2.at<double>(2,0) = Pn2.at<double>(2,0);
    Qn2.at<double>(2,1) = Pn2.at<double>(2,1);
    Qn2.at<double>(2,2) = Pn2.at<double>(2,2);

    //The transform below is used for inverse mapping the images
    Transform1 = Qn1*(Qo1.inv());
    Transform2 = Qn2*(Qo2.inv());
    
    std::cout << "Transform1: " << Transform1 << std::endl;
    std::cout << "Transform2: " << Transform2 << std::endl;
    
    return;
}

//using OpenCV the images are imported and converted to grayscale
void LoadImagePoints(std::string image_dir, cv::Mat &image, cv::Mat &gray_image) {
    
    image = cv::imread(image_dir);
    cv::cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);
    // These images can be blurred using the Filter function
    return;
}

void Harris(cv::Mat gray_image){
     cv::Mat BlurMagx(gray_image.rows, gray_image.cols, CV_8UC3), BlurMagy(gray_image.rows, gray_image.cols, CV_8UC3), BlurMagxy(gray_image.rows, gray_image.cols, CV_8UC3), SobelBlur(gray_image.rows, gray_image.cols, CV_8U), SobelMagx(gray_image.rows, gray_image.cols, cv::DataType<double>::type), SobelMagy(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Dummy(gray_image.rows, gray_image.cols, CV_8U);
    
    double min, max;
    //First the image is normalised
    cv::minMaxLoc(gray_image, &min, &max);
    cv::Mat I = gray_image/max;
    
    //The image is passed throigh the sobel filter to find the X and y derivative of the image
    Filter(I, SobelMagx, SobelMagy, "Sobel", "Gray");
    cv::imwrite("../Sobel/SobTestMagx.png", SobelMagx);
    cv::imwrite("../Sobel/SobTestMagy.png", SobelMagy);
    
    //The derivates are multiplied together
    cv::Mat Ixx = SobelMagx.mul(SobelMagx);
    cv::Mat Ixy = SobelMagx.mul(SobelMagy);
    cv::Mat Iyy = SobelMagy.mul(SobelMagy);
//    std::cout << Ixx.type() << std::endl;
    
    //Each derivative image is passed through the gaussian blur function
    cv::Mat Gxx(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Gxy(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Gyy(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Gxy2(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Gsum2(gray_image.rows, gray_image.cols, cv::DataType<double>::type);
    Filter(Ixx, Gxx, Dummy, "Blur", "Gray");
    Filter(Ixy, Gxy, Dummy, "Blur", "Gray");
    Filter(Iyy, Gyy, Dummy, "Blur", "Gray");
    
    //New matrices are set up to be used for the final calculation
    for(int i = 0; i < gray_image.rows; i ++){
        for(int x = 0; x < gray_image.cols; x++){
            Gxy2.at<double>(i,x) = pow(Gxy.at<double>(i,x),2);
        }
    }
    
    for(int i = 0; i < gray_image.rows; i ++){
        for(int x = 0; x < gray_image.cols; x++){
            Gsum2.at<double>(i,x) = pow(Gxx.at<double>(i,x) + Gyy.at<double>(i,x),2);
        }
    }
    //The harris corner detection is performed by assigning each pixel a score and then normalising
    cv::Mat H(gray_image.rows, gray_image.cols, cv::DataType<double>::type), Hdense(gray_image.rows, gray_image.cols, cv::DataType<double>::type), loc(gray_image.rows, gray_image.cols, cv::DataType<double>::type);
    H = (Gxx.mul(Gyy) - Gxy2) - 0.06*Gsum2;
    cv::minMaxLoc(H, &min, &max);
    Hdense = H/max;

    cv::Mat Copy;
    gray_image.copyTo(Copy);
    //Searching through the image, any points with an H score above a certain threshold is highlighted
    for(int i = 0; i < gray_image.rows; i ++){
        for(int x = 0; x < gray_image.cols; x++){
            if(Hdense.at<double>(i,x) >= 0.2){
                cv::Point One1(x - 3,i- 3);
                cv::Point Two1(x + 3,i + 3);
                cv::rectangle(Copy, One1, Two1, 255, 1);
            }
        }
    }
    
    cv::imwrite("../Copy.png", Copy);
    
    
    return;
}

// Find the mimimum value
void minCheck(cv::Mat Point, double& Tempx, double& Tempy){
    if(Point.at<double>(0,0) < Tempx){
        Tempx = Point.at<double>(0,0);
    }
    if(Point.at<double>(1,0) < Tempy){
        Tempy = Point.at<double>(1,0);
    }
    
}

// Find the maximum value
void maxCheck(cv::Mat Point, double& Tempx, double& Tempy){
    if(Point.at<double>(0,0) > Tempx){
        Tempx = Point.at<double>(0,0);
    }
    if(Point.at<double>(1,0) > Tempy){
        Tempy = Point.at<double>(1,0);
    }
    
}

//Calculate the new boudning box for the rectified images
void Adjust(cv::Mat T, double& minx, double& miny, double& maxx, double& maxy, int recCol, int recRow){
    cv::Mat Point(3,1, cv::DataType<double>::type);
    cv::Mat newPoint(3,1, cv::DataType<double>::type);

    //Multiply each point with the rectification transform
    for(int row = 0; row < recRow; row++){
        for(int col = 0; col < recCol ; col++){
            Point.at<double>(0) = row;
            Point.at<double>(1) = col;
            Point.at<double>(2) = 1;
            newPoint = T*Point;
            minCheck(newPoint, minx, miny);
            maxCheck(newPoint, maxx, maxy);
        }
    }
        std::cout << "min x: " << minx << std::endl;
        std::cout << "min y: " << miny << std::endl;
        std::cout << "max x: " << maxx << std::endl;
        std::cout << "max y: " << maxx << std::endl;
    
    return;
}

//Billinear interpolation in order to get more accurate resuls from the inverse mapping
double BilinearInterpolation(double x1, double x2, double y1, double y2, double x, double y,  double q11,double q12,double q21,double q22 )
{
    double x2x1, y1y2, x2x, y1y, yy2, xx1;
    x2x1 = x2 - x1;
    y1y2 = y1 - y2;
    x2x = x2 - x;
    y1y = y1 - y;
    yy2 = y - y2;
    xx1 = x - x1;
    double I1 = ((x2x)/(x2x1))*q12 + ((xx1)/(x2x1))*q22;
    //    cout << "I1: " << I1 << endl;
    double I2 = ((x2x)/(x2x1))*q11 + ((xx1)/(x2x1))*q21;
    //    cout << "I2: " << I2 << endl;
    double I3 = ((y1y)/(y1y2))*I1 + ((yy2)/(y1y2))*I2;
    //    cout << "I3: " << I3 << endl;
    return I3;
}

void inverseMapping(cv::Mat leftTransform, cv::Mat rightTransform, cv::Mat Big_left, cv::Mat Big_right, cv::Mat &newImage1, cv::Mat &newImage2, double colOffset, double rowOffset, int recCol, int recRow){
    cv::Mat Point(3,1, cv::DataType<double>::type);
    cv::Mat newPoint1(3,1, cv::DataType<double>::type);
    cv::Mat newPoint2(3,1, cv::DataType<double>::type);
    
    //Offset the images so that the inverse mapping does not refer to negative pixel value in the original image
    cv::Mat Boundry(3,3, cv::DataType<double>::type);
    Boundry.at<double>(0,0) = 1;
    Boundry.at<double>(0,1) = 0;
    Boundry.at<double>(0,2) = - colOffset;
    Boundry.at<double>(1,0) = 0;
    Boundry.at<double>(1,1) = 1;
    Boundry.at<double>(1,2) = - rowOffset;
    Boundry.at<double>(2,0) = 0;
    Boundry.at<double>(2,1) = 0;
    Boundry.at<double>(2,2) = 1;
    
    for(int row = 0; row < recRow; row++){
        for(int col = 0; col < recCol ; col++){
            Point.at<double>(0) = row;
            Point.at<double>(1) = col;
            Point.at<double>(2) = 1;
            
            // Find the correspnding pixel position in the original image
            newPoint1 = leftTransform*(Boundry.inv())*Point;
            newPoint2 = rightTransform*(Boundry.inv())*Point;
            
            // The nearest row and colum is found for the interpolation
            double col11 = floor(newPoint1.at<double>(1,0));
            double col12 = ceil(newPoint1.at<double>(1,0));
            double row11 = ceil(newPoint1.at<double>(0,0));
            double row12 = floor(newPoint1.at<double>(0,0));
            double col21 = floor(newPoint2.at<double>(1,0));
            double col22 = ceil(newPoint2.at<double>(1,0));
            double row21 = ceil(newPoint2.at<double>(0,0));
            double row22 = floor(newPoint2.at<double>(0,0));

            //Checks if the inverse mapping is reffering to a point that doesnt exist and makes it 0
            if(col12 >= Big_left.cols || row11 >= Big_left.rows || col11 < 0 || row12 < 0){
                newImage1.at<cv::Vec3b>(row, col)[0] = 0;
                newImage1.at<cv::Vec3b>(row, col)[1] = 0;
                newImage1.at<cv::Vec3b>(row, col)[2] = 0;
            }else{
                //Interpolates and sets the new value of the image
                newImage1.at<cv::Vec3b>(row, col)[0] = BilinearInterpolation(col11, col12, row11, row12, newPoint1.at<double>(1,0), newPoint1.at<double>(0,0), Big_left.at<cv::Vec3b>(row11,col11)[0], Big_left.at<cv::Vec3b>(row12,col11)[0], Big_left.at<cv::Vec3b>(row11,col12)[0], Big_left.at<cv::Vec3b>(row12,col12)[0]);
                newImage1.at<cv::Vec3b>(row, col)[1] = BilinearInterpolation(col11, col12, row11, row12, newPoint1.at<double>(1,0), newPoint1.at<double>(0,0), Big_left.at<cv::Vec3b>(row11,col11)[1], Big_left.at<cv::Vec3b>(row12,col11)[1], Big_left.at<cv::Vec3b>(row11,col12)[1], Big_left.at<cv::Vec3b>(row12,col12)[1]);
                newImage1.at<cv::Vec3b>(row, col)[2] = BilinearInterpolation(col11, col12, row11, row12, newPoint1.at<double>(1,0), newPoint1.at<double>(0,0), Big_left.at<cv::Vec3b>(row11,col11)[2], Big_left.at<cv::Vec3b>(row12,col11)[2], Big_left.at<cv::Vec3b>(row11,col12)[2], Big_left.at<cv::Vec3b>(row12,col12)[2]);
            }

            //Checks if the inverse mapping is reffering to a point that doesnt exist and makes it 0
            if(col22 >= Big_right.cols || row21 >= Big_left.rows || col21 < 0 || row22 < 0){
                newImage2.at<cv::Vec3b>(row, col)[0] = 0;
                newImage2.at<cv::Vec3b>(row, col)[1] = 0;
                newImage2.at<cv::Vec3b>(row, col)[2] = 0;
            }else{
                //Interpolates and sets the new value of the image
                newImage2.at<cv::Vec3b>(row, col)[0] = BilinearInterpolation(col21, col22, row21, row22, newPoint2.at<double>(1,0), newPoint2.at<double>(0,0), Big_right.at<cv::Vec3b>(row21,col21)[0], Big_right.at<cv::Vec3b>(row22,col21)[0], Big_right.at<cv::Vec3b>(row21,col22)[0], Big_right.at<cv::Vec3b>(row22,col22)[0]);
                newImage2.at<cv::Vec3b>(row, col)[1] = BilinearInterpolation(col21, col22, row21, row22, newPoint2.at<double>(1,0), newPoint2.at<double>(0,0), Big_right.at<cv::Vec3b>(row21,col21)[1], Big_right.at<cv::Vec3b>(row22,col21)[1], Big_right.at<cv::Vec3b>(row21,col22)[1], Big_right.at<cv::Vec3b>(row22,col22)[1]);
                newImage2.at<cv::Vec3b>(row, col)[2] = BilinearInterpolation(col21, col22, row21, row22, newPoint2.at<double>(1,0), newPoint2.at<double>(0,0), Big_right.at<cv::Vec3b>(row21,col21)[2], Big_right.at<cv::Vec3b>(row22,col21)[2], Big_right.at<cv::Vec3b>(row21,col22)[2], Big_right.at<cv::Vec3b>(row22,col22)[2]);
            }
        }
    }
    cv::imwrite("../Range_Images/NLeft_Rectified.png", newImage1);
    cv::imwrite("../Range_Images/NRight_Rectified.png", newImage2);
    
    return;
}

//Find the average of a matrix
double Average(cv::Mat Window){
    double avg = 0.0, sum = 0.0;
    
    for(int row = 0; row < Window.rows; row ++){
        for(int col = 0; col < Window.cols; col ++){
            sum += Window.at<double>(row,col);
        }
    }
    avg = sum/(pow(Window.rows,2));
    
    return avg;
}

//Normalised cross correlation used for the disparity map generateion through correspondance matching
void NCC(int WinSize, int MinDisp, int MaxDisp, cv::Mat I1, cv::Mat I2, int Blur, cv::Mat &DispIm){
    double temp1 = 0, temp2 = 0, mean1, mean2, tempmin;
    int rowIndex = 0, Offset = 0, index = 0;
    std::vector<cv::Mat> Im2Mat;
    std::vector<double> NCCvalues;
    
    cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
    
    //Isolates a window in the left camera image
    for(int row = WinSize; row < I1.rows - WinSize; row++){
        for(int col = WinSize; col < I1.cols - WinSize; col++){
            rowIndex = row + Offset;
            NCCvalues.clear();
            temp1 = 0.0;
            for(int x = 0; x < 2*WinSize+1; x++){
                for(int y = 0; y < 2*WinSize+1; y++){
                    Im1Win.at<double>(x,y) = (double)I1.at<uchar>(row - WinSize + x, col - WinSize + y);
                    temp1 += (double)I1.at<uchar>(row - WinSize + x, col - WinSize + y);
                }
            }
            
            //if the average of the window is 0 then it is assumed to be background and therefore ignored as only the baby needs to be 3D
            if(temp1 == 0){
                DispIm.at<double>(row,col) = 0.0;
                continue;
            }
            
            //For every allowed disparity value isolate a window in the second image
            for(int i = MinDisp; i < MaxDisp; i++){
                temp2 = 0.0;
                mean1 = 0.0;
                mean2 = 0.0;
                for(int x = 0; x < 2*WinSize+1; x++){
                    for(int y = 0; y < 2*WinSize+1; y++){
                        Im2Win.at<double>(x,y) = (double)I2.at<uchar>(row - WinSize + x, col - WinSize + y + i);
                        temp2 += (double)I2.at<uchar>(rowIndex - WinSize + x, col - WinSize + y + i);
                    }
                }
                // Calculate the average of the isolated windows
                mean1 = temp1/(pow(2*WinSize+1, 2));
                mean2 = temp2/(pow(2*WinSize+1, 2));
                
                //If the right image window has an average of 0 it is assumed to be background and then ignored
                if(temp2 == 0){
                    NCCvalues.push_back(-1);
                    continue;
                }
                double testing = 0.0;
                //The normalised cross correlation is worked out here
                testing = (Im1Win.dot(Im2Win) - (pow(2*WinSize+1, 2)*mean1*mean2))/(sqrt(pow(norm(Im1Win), 2) - pow(2*WinSize+1, 2)*pow(mean1, 2))*sqrt(pow(norm(Im2Win), 2) - pow(2*WinSize+1, 2)*pow(mean2, 2)));
                if(testing > 1 || testing < -1){
                    testing = -1;
                }
                
                NCCvalues.push_back(testing);
                
            }
            
            // The current NCC value is taken and subratec from 1. The answer closet to 0 for all possible disparity values is the chosen disparity value
            tempmin = 2;
            double tempval = 0;
            for(int xb = 0; xb < NCCvalues.size(); xb ++){
                if(NCCvalues[xb] < 0){
                    tempval = 1 + abs(NCCvalues[xb]);
                }
                else if(NCCvalues[xb] > 1 || NCCvalues[xb] < -1){
                    std::cout << "error" << std::endl;
                }
                else{
                    tempval = 1 - NCCvalues[xb];
                    
                }
                if(tempval < tempmin){
                    tempmin = tempval;
                    index = xb + MinDisp;
                }
            }
            
            double scale = (double)MaxDisp/255;
            double scaled = (double)index/scale;
            DispIm.at<double>(row,col) = scaled;
            
        }
        std::cout << "NCC-Row: " << row << std::endl;
    }
    
    if(Blur == 0){
        cv::imwrite("../Range_Images/DispIm/NCCLeft_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I1);
        cv::imwrite("../Range_Images/DispIm/NCCRight_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I2);
        cv::imwrite("../Range_Images/DispIm/NCCDispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", DispIm);
        std::cout << "finished Disparity Map" << std::endl;
    }
    else if(Blur == 1){
        cv::imwrite("../Range_Images/DispIm/NCCLeft_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I1);
        cv::imwrite("../Range_Images/DispIm/NCCRight_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I2);
        cv::imwrite("../Range_Images/DispIm/NCCDispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", DispIm);
        std::cout << "finished NCC Disparity Map" << std::endl;
        std::cout << "DispIm: " << DispIm << std::endl;
    }
    
    return;
}

// Sum fo absolute differences method of correspondance matching. Not used as the NCC is robust against contrast and brightness differences and therefore performs better in this case
void SAD(int WinSize, int MinDisp, int MaxDisp, cv::Mat I1, cv::Mat I2, int Blur, cv::Mat &DispIm){
    int Offset = 0, index = 0;
    double SADWin = 0.0, temp = 0.0, Im1Avg = 0.0;
    int rowIndex = 0;
    cv::Mat leftIm(1230, 1350, CV_8UC3), rightIm(1230, 1350, CV_8UC3);
    std::vector<cv::Mat> Im2Mat;
    std::vector<double> SADvalues;
    std::cout << "Starting SAD Disp map." << std::endl;
    //    std::cout << I1.type() << std::endl;
    if(Blur == 0){
        I1.copyTo(leftIm);
        I2.copyTo(rightIm);
    }
    else if(Blur == 1){
//        SharpeningFilter("Gaus", 0, I1, leftIm);
//        SharpeningFilter("Gaus", 1, I2, rightIm);
    }
    
    for(int row = WinSize; row < 1230 - WinSize; row++){
        for(int col = WinSize; col < 1350 - WinSize; col++){
            rowIndex = row + Offset;
            SADvalues.clear();
            
            cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrix(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
            
            for(int x = 0; x < 2*WinSize+1; x++){
                for(int y = 0; y < 2*WinSize+1; y++){
                    Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col - WinSize + y);
                }
            }
            Im1Avg = Average(Im1Win);
            
            if(Im1Avg == 0){
                DispIm.at<double>(row,col) = 0.0;
                continue;
            }
            
            for(int i = MinDisp; i < MaxDisp; i++){
                for(int x = 0; x < 2*WinSize+1; x++){
                    for(int y = 0; y < 2*WinSize+1; y++){
                        
                        Im2Win.at<double>(x,y) = I2.at<double>(rowIndex - WinSize + x, col - WinSize + y + i);
                        
                    }
                }
                //                cout << Im2Win << endl;
                SADMatrix = abs(Im1Win - Im2Win);
                
                SADWin = 0;
                for(int x = 0; x < 2*WinSize+1; x++){
                    for(int y = 0; y < 2*WinSize+1; y++){
                        SADWin += SADMatrix.at<double>(x,y);
                    }
                }
                
                SADvalues.push_back(SADWin);
                
            }
            
            temp = SADvalues[0];
            
            for(int x = 0; x < SADvalues.size(); x ++){
                if (SADvalues[x] <= temp ) {
                    temp = SADvalues[x];
                    index = x + MinDisp;
                }
            }
            double scale = MaxDisp/255;
            double scaled = (double)index/scale;
            DispIm.at<double>(row,col) = scaled;
            
        }
        std::cout << "SAD-Row: " << row << std::endl;
    }
    
    if(Blur == 0){
        cv::imwrite("../Range_Images/DispIm/Left_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I1);
        cv::imwrite("../Range_Images/DispIm/Right_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I2);
        cv::imwrite("../Range_Images/DispIm/DispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", DispIm);
        std::cout << "finished NCC Disparity Map" << std::endl;
    }
    
    else if(Blur == 1){
        cv::imwrite("../Range_Images/DispIm/Left_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I1);
        cv::imwrite("../Range_Images/DispIm/Right_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I2);
        cv::imwrite("../Range_Images/DispIm/DispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", DispIm);
        std::cout << "finished SAD Blurred Disparity Map" << std::endl;
        std::cout << "DispIm: " << DispIm << std::endl;
    }
    
    return;
}

//Local binary patterning. This was used in an attempt to add texture to the textureless regions of the images. This did not work as differences in the brightness of the two images resulted in slightly different patters which reduced the number of succesful stereo correspondances
void LBP(int WinSize, cv::Mat originalIm, cv::Mat &texturedIm){
    cv::Mat gray_image(originalIm.rows, originalIm.cols, CV_8U);
    
    //    cv::cvtColor(originalIm, gray_image, cv::COLOR_BGR2GRAY);
    
    cv::Mat Weights(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
    
    Weights.at<double>(0,0) = 1;
    Weights.at<double>(0,1) = 2;
    Weights.at<double>(0,2) = 4;
    Weights.at<double>(1,0) = 128;
    Weights.at<double>(1,1) = 0;
    Weights.at<double>(1,2) = 8;
    Weights.at<double>(2,0) = 64;
    Weights.at<double>(2,1) = 32;
    Weights.at<double>(2,2) = 16;
    std::cout << "Weights: " << Weights << std::endl;
    for(int row = WinSize; row < originalIm.rows - WinSize; row++){
        for(int col = WinSize; col < originalIm.cols - WinSize; col++){
            
            cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Binary(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
            
            for(int x = 0; x < 2*WinSize+1; x++){
                for(int y = 0; y < 2*WinSize+1; y++){
                    Im1Win.at<double>(x,y) = (double)originalIm.at<uchar>(row - WinSize + x, col - WinSize + y);
                }
            }
            
            double Threshold = Im1Win.at<double>(WinSize, WinSize);
            double sum = 0.0;
            if(Threshold == 0){
                sum = 0.0;
            }
            else{
                //             std::cout << "Window: " << Im1Win << std::endl;
                //            std::cout << "Threshold: " << Threshold << std::endl;
                
                for(int x = 0; x < 2*WinSize+1; x++){
                    for(int y = 0; y < 2*WinSize+1; y++){
                        if (Im1Win.at<double>(x, y) < Threshold) {
                            Binary.at<double>(x, y) = 0.0;
                        }
                        else if(Im1Win.at<double>(x, y) >= Threshold){
                            Binary.at<double>(x, y) = 1.0;
                        }
                        sum += Binary.at<double>(x,y)*Weights.at<double>(x,y);
                    }
                }
            }
            //            std::cout << "Sum: " << sum << std::endl;
            texturedIm.at<uchar>(row,col) = (uchar)sum;
        }
    }
    
    return;
}

//Used to get the rescaling paramter for the show wolf algorithm below
int getRescale(double std, double stdPrime, double f){
    double s = std;
    
    if (std < stdPrime){
        if(std < f){
            s = stdPrime*(std/f);
        }
        else{
            s = stdPrime;
        }
    }
    return s;
}

//Shadow Wolf algortimn which enhances fine details in the image. (This is originally used for clearing an image from a dash cam which is in fog. It works very well to add texture but unfortunatly due to the intenisty value differences between the two images(right camera takes brighter pictures) the texture added is not exactly the same in both images and therefore the number of succesful matches is reduced.) This is not currently used
void shadowWolf(int WinSize, double dRange, double var, double speed, cv::Mat originalIm, cv::Mat &Shadow){
    cv::Mat grayIm(originalIm.rows, originalIm.cols, CV_8U);
    double Im1Avg = 0.0, s = 0.0, vmin = 0, vmax = 255;
    
    //    cv::cvtColor(originalIm, grayIm, cv::COLOR_BGR2GRAY);
    
    for(int row = WinSize; row < originalIm.rows - WinSize; row++){
        for(int col = WinSize; col < originalIm.cols - WinSize; col++){
            
            cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
            
            for(int x = 0; x < 2*WinSize+1; x++){
                for(int y = 0; y < 2*WinSize+1; y++){
                    Im1Win.at<double>(x,y) = (double)originalIm.at<uchar>(row - WinSize + x, col - WinSize + y);
                }
            }
            Im1Avg = Average(Im1Win);
            
            if(Im1Avg == 0){
                Shadow.at<uchar>(row,col) = 0.0;
                continue;
            }
            double sum = 0.0;
            for(int x = 0; x < 2*WinSize+1; x++){
                for(int y = 0; y < 2*WinSize+1; y++){
                    sum += pow(Im1Win.at<double>(x, y) - Im1Avg, 2);
                }
            }
            double std = 0.0;
            
            std = sqrt(sum/(pow(2*WinSize+1, 2)));
            
            double zScore = (Im1Win.at<double>(WinSize, WinSize) - Im1Avg)/std;
            
            s = getRescale(std, var, speed);
            
            double meanPrime = dRange + (Im1Avg - vmin)*(((vmax - dRange)-(vmin + dRange))/(vmax - vmin));
            
            double intensity = meanPrime + (zScore*s);
            
            Shadow.at<uchar>(row,col) = (uchar)intensity;
        }
        std::cout << "SW-Row: " << row << std::endl;
    }
    return;
}

//Find maximum value in a matrix
double findMax(cv::Mat T){
    double max = 0.0;
    
    for(int row = 0; row < T.rows; row ++){
        for(int col = 0; col < T.cols; col ++){
            if (T.at<double>(row,col) > max) {
                max = T.at<double>(row,col);
            }
        }
    }
    return max;
}

//Another method which can be used for calculating the rectification transform
//void RectifyingTransformTest2(cv::Mat Po1, cv::Mat Po2, cv::Mat R1, cv::Mat R2, cv::Mat RelR, cv::Mat RelT, cv::Mat C1, cv::Mat C2,
//                             cv::Mat K1, cv::Mat K2, cv::Mat &Transform1f, cv::Mat &Transform2f, cv::Mat &Transform1r, cv::Mat &Transform2r){
//    cv::Mat Qo1(3,3, cv::DataType<double>::type), Qo2(3,3, cv::DataType<double>::type), Rprime(3,3, cv::DataType<double>::type),
//    Ex1(4,4, cv::DataType<double>::type), Ex2(4,4, cv::DataType<double>::type), Pn1(3,4, cv::DataType<double>::type),
//    Pn2(3,4, cv::DataType<double>::type), Qn1(3,3, cv::DataType<double>::type), Qn2(3,3, cv::DataType<double>::type),Rc1(3,1, cv::DataType<double>::type), Rc2(3,1, cv::DataType<double>::type),
//    rpx, rpy, rpz, Kprime(3,4, cv::DataType<double>::type, cv::Scalar(0));
//
//    rpx = (C1-C2)/norm(C1-C2, cv::NORM_L2);
//
//    rpy = (R1.row(2)).t().cross(rpx);
//
//    rpz = (rpx).cross(rpy);
//
//    rpx = rpx.t()/(norm(rpx, cv::NORM_L2));
//    rpy = rpy.t()/(norm(rpy, cv::NORM_L2));
//    rpz = rpz.t()/(norm(rpz, cv::NORM_L2));
//
//    std::cout << "rpx: " << rpx << std::endl;
//    std::cout << "rpy: " << rpy << std::endl;
//    std::cout << "rpz: " << rpz << std::endl;
//
//    Rprime.at<double>(0,0) = rpx.at<double>(0);
//    Rprime.at<double>(0,1) = rpx.at<double>(1);
//    Rprime.at<double>(0,2) = rpx.at<double>(2);
//    Rprime.at<double>(1,0) = rpy.at<double>(0);
//    Rprime.at<double>(1,1) = rpy.at<double>(1);
//    Rprime.at<double>(1,2) = rpy.at<double>(2);
//    Rprime.at<double>(2,0) = rpz.at<double>(0);
//    Rprime.at<double>(2,1) = rpz.at<double>(1);
//    Rprime.at<double>(2,2) = rpz.at<double>(2);
//
//    std::cout << "Rprime: " << Rprime << std::endl;
//
//    Kprime = (K1 + K2)/2;
//    Kprime.at<double>(0,1) = 0;
//    std::cout << "Kprime: " << Kprime << std::endl;
//
//    Transform1f = Kprime*Rprime*R1.inv()*K1.inv();
//    Transform2f = Kprime*Rprime*R2.inv()*K2.inv();
//    std::cout << "Transform1f: " << Transform1f << std::endl;
//    std::cout << "Transform2f: " << Transform2f << std::endl;
//
//    Transform1r = K1*R1*(Rprime.inv())*(Kprime.inv());
//    Transform2r = K2*R2*(Rprime.inv())*(Kprime.inv());
//
//    std::cout << "Transform1r: " << Transform1r << std::endl;
//    std::cout << "Transform2r: " << Transform2r << std::endl;
//
//    return;
//}


// Calculates the inverse mapping rectification transform. This particular method i tried did not work at all.
//void RectifyingTransformNew(cv::Mat Po1, cv::Mat Po2, cv::Mat R1, cv::Mat R2, cv::Mat RelR, cv::Mat RelT, cv::Mat C1, cv::Mat C2,
//                            cv::Mat K1, cv::Mat K2, cv::Mat &Transform1f, cv::Mat &Transform2f, cv::Mat &Transform1r, cv::Mat &Transform2r){
//    cv::Mat Qo1(3,3, cv::DataType<double>::type), Qo2(3,3, cv::DataType<double>::type), Rprime(3,3, cv::DataType<double>::type),
//    Ex1(3,4, cv::DataType<double>::type), Ex2(3,4, cv::DataType<double>::type), Pn1(3,4, cv::DataType<double>::type),
//    Pn2(3,4, cv::DataType<double>::type), Qn1(3,3, cv::DataType<double>::type), Qn2(3,3, cv::DataType<double>::type),Rc1(3,1, cv::DataType<double>::type), Rc2(3,1, cv::DataType<double>::type),
//    rpx, rpy, rpz, Kprime;
//
//    Qo1.at<double>(0,0) = Po1.at<double>(0,0);
//    Qo1.at<double>(0,1) = Po1.at<double>(0,1);
//    Qo1.at<double>(0,2) = Po1.at<double>(0,2);
//    Qo1.at<double>(1,0) = Po1.at<double>(1,0);
//    Qo1.at<double>(1,1) = Po1.at<double>(1,1);
//    Qo1.at<double>(1,2) = Po1.at<double>(1,2);
//    Qo1.at<double>(2,0) = Po1.at<double>(2,0);
//    Qo1.at<double>(2,1) = Po1.at<double>(2,1);
//    Qo1.at<double>(2,2) = Po1.at<double>(2,2);
//
//    Qo2.at<double>(0,0) = Po2.at<double>(0,0);
//    Qo2.at<double>(0,1) = Po2.at<double>(0,1);
//    Qo2.at<double>(0,2) = Po2.at<double>(0,2);
//    Qo2.at<double>(1,0) = Po2.at<double>(1,0);
//    Qo2.at<double>(1,1) = Po2.at<double>(1,1);
//    Qo2.at<double>(1,2) = Po2.at<double>(1,2);
//    Qo2.at<double>(2,0) = Po2.at<double>(2,0);
//    Qo2.at<double>(2,1) = Po2.at<double>(2,1);
//    Qo2.at<double>(2,2) = Po2.at<double>(2,2);
//
//    cv::Mat tempu1(3,1, cv::DataType<double>::type), tempu2(3,1, cv::DataType<double>::type), tempv1(3,1, cv::DataType<double>::type);
//
//    tempu1.at<double>(0) = Po1.at<double>(0, 0);
//    tempu1.at<double>(1) = Po1.at<double>(0, 1);
//    tempu1.at<double>(2) = Po1.at<double>(0, 2);
////    std::cout << Po1 << std::endl;
////    std::cout << tempu1 << std::endl;
//
//    tempu2.at<double>(0) = Po1.at<double>(2, 0);
//    tempu2.at<double>(1) = Po1.at<double>(2, 1);
//    tempu2.at<double>(2) = Po1.at<double>(2, 2);
//
//    tempv1.at<double>(0) = Po1.at<double>(1, 0);
//    tempv1.at<double>(1) = Po1.at<double>(1, 1);
//    tempv1.at<double>(2) = Po1.at<double>(1, 2);
//
////    cv::Mat Out1(tempu1.rows, tempu2.rows, cv::DataType<double>::type), Out2(tempu1.rows, tempv1.rows, cv::DataType<double>::type);
////    OuterProduct(tempu1, tempu2, Out1);
////    OuterProduct(tempu1, tempv1, Out2);
////    std::cout << "Out1" << Out1 << std::endl;
////    std::cout << "Out2" << Out2 << std::endl;
//
//    double au = norm(tempu1.cross(tempu2), cv::NORM_L2);
//    double av = norm(tempv1.cross(tempu2), cv::NORM_L2);
//    std::cout << "au: " << au << std::endl;
//    std::cout << "av: " << av << std::endl;
//
//    cv::Mat fl(3,1, cv::DataType<double>::type), fr(3,1, cv::DataType<double>::type);
//
//    fl.at<double>(0) = Po1.at<double>(2, 0);
//    fl.at<double>(1) = Po1.at<double>(2, 1);
//    fl.at<double>(2) = Po1.at<double>(2, 2);
//
//    fr.at<double>(0) = Po2.at<double>(2, 0);
//    fr.at<double>(1) = Po2.at<double>(2, 1);
//    fr.at<double>(2) = Po2.at<double>(2, 2);
//
//    cv::Mat nn = fl.cross(fr);
//    std::cout << "fl: " << fl << std::endl;
//    std::cout << "fr: " << fr << std::endl;
//    std::cout << "nn: " << nn << std::endl;
//
//    cv::Mat A(4,3, cv::DataType<double>::type);
//
//    A.at<double>(0,0) = C1.at<double>(0);
//    A.at<double>(1,0) = C1.at<double>(1);
//    A.at<double>(2,0) = C1.at<double>(2);
//    A.at<double>(0,1) = C2.at<double>(0);
//    A.at<double>(1,1) = C2.at<double>(1);
//    A.at<double>(2,1) = C2.at<double>(2);
//    A.at<double>(0,2) = nn.at<double>(0);
//    A.at<double>(1,2) = nn.at<double>(1);
//    A.at<double>(2,2) = nn.at<double>(2);
//    A.at<double>(3,0) = 1;
//    A.at<double>(3,1) = 1;
//    A.at<double>(3,2) = 0;
//
//    std::cout << "A: " << A << std::endl;
//    cv::Mat Al = A.t();
//    std::cout << "A': " << A.t() << std::endl;
//    cv::Mat U(3, 3, cv::DataType<double>::type);
//    cv::Mat S(1, 4, cv::DataType<double>::type);
//    cv::Mat Vh(4, 4, cv::DataType<double>::type);
//
//    cv::SVD::compute(Al , S, U, Vh, cv::SVD::FULL_UV );
//
//    std::cout << "U: " << U << std::endl;
//    std::cout << "S: " << S << std::endl;
//    std::cout << "Vh: " << Vh << std::endl;
//
//    cv::Mat Vhtemp(3,1, cv::DataType<double>::type);
//    Vhtemp.at<double>(0) = Vh.at<double>(0, 3);
//    Vhtemp.at<double>(1) = Vh.at<double>(1, 3);
//    Vhtemp.at<double>(2) = Vh.at<double>(2, 3);
//    double r = 1/norm(Vhtemp, cv::NORM_L2);
//    std::cout << "r: " << r << std::endl;
//    cv::Mat a3(4,1, cv::DataType<double>::type);
//    a3 = r*Vh.col(3);
//    std::cout << "a3: " << a3 << std::endl;
//
//    cv::Mat B(4,3, cv::DataType<double>::type);
//
//    B.at<double>(0,0) = C1.at<double>(0);
//    B.at<double>(1,0) = C1.at<double>(1);
//    B.at<double>(2,0) = C1.at<double>(2);
//    B.at<double>(0,1) = C2.at<double>(0);
//    B.at<double>(1,1) = C2.at<double>(1);
//    B.at<double>(2,1) = C2.at<double>(2);
//    B.at<double>(0,2) = a3.at<double>(0);
//    B.at<double>(1,2) = a3.at<double>(1);
//    B.at<double>(2,2) = a3.at<double>(2);
//    B.at<double>(3,0) = 1;
//    B.at<double>(3,1) = 1;
//    B.at<double>(3,2) = 0;
//
//    std::cout << "B: " << B << std::endl;
//    cv::Mat Bl = B.t();
//    std::cout << "B': " << B.t() << std::endl;
//    cv::Mat Ub(3, 3, cv::DataType<double>::type);
//    cv::Mat Sb(1, 4, cv::DataType<double>::type);
//    cv::Mat Vhb(4, 4, cv::DataType<double>::type);
//
//    cv::SVD::compute(Bl , Sb, Ub, Vhb, cv::SVD::FULL_UV );
//
//    std::cout << "Ub : " << Ub << std::endl;
//    std::cout << "Sb : " << Sb << std::endl;
//    std::cout << "Vhb : " << Vhb << std::endl;
//
//    cv::Mat Vhtempb(3,1, cv::DataType<double>::type);
//    Vhtempb.at<double>(0) = Vhb.at<double>(0, 3);
//    Vhtempb.at<double>(1) = Vhb.at<double>(1, 3);
//    Vhtempb.at<double>(2) = Vhb.at<double>(2, 3);
//    double rb = norm(av,cv::NORM_L2)/norm(Vhtempb, cv::NORM_L2);
//    std::cout << "rb: " << rb << std::endl;
//    cv::Mat a2(4,1, cv::DataType<double>::type);
//    a2 = rb*Vhb.col(3);
//    std::cout << "a2: " << a2 << std::endl;
//
//    cv::Mat C(4,3, cv::DataType<double>::type);
//
//    C.at<double>(0,0) = C1.at<double>(0);
//    C.at<double>(1,0) = C1.at<double>(1);
//    C.at<double>(2,0) = C1.at<double>(2);
//    C.at<double>(0,1) = a2.at<double>(0);
//    C.at<double>(1,1) = a2.at<double>(1);
//    C.at<double>(2,1) = a2.at<double>(2);
//    C.at<double>(0,2) = a3.at<double>(0);
//    C.at<double>(1,2) = a3.at<double>(1);
//    C.at<double>(2,2) = a3.at<double>(2);
//    C.at<double>(3,0) = 1;
//    C.at<double>(3,1) = 0;
//    C.at<double>(3,2) = 0;
//
//    std::cout << "C: " << C << std::endl;
//    cv::Mat Cl = C.t();
//    std::cout << "C': " << C.t() << std::endl;
//    cv::Mat Uc(3, 3, cv::DataType<double>::type);
//    cv::Mat Sc(1, 4, cv::DataType<double>::type);
//    cv::Mat Vhc(4, 4, cv::DataType<double>::type);
//
//    cv::SVD::compute(Cl , Sc, Uc, Vhc, cv::SVD::FULL_UV );
//
//    std::cout << "Uc : " << Uc << std::endl;
//    std::cout << "Sc : " << Sc << std::endl;
//    std::cout << "Vhc : " << Vhc << std::endl;
//
//    cv::Mat Vhtempc(3,1, cv::DataType<double>::type);
//    Vhtempc.at<double>(0) = Vhc.at<double>(0, 3);
//    Vhtempc.at<double>(1) = Vhc.at<double>(1, 3);
//    Vhtempc.at<double>(2) = Vhc.at<double>(2, 3);
//    double rc = norm(au,cv::NORM_L2)/norm(Vhtempc, cv::NORM_L2);
//    std::cout << "rc: " << rc << std::endl;
//    cv::Mat a1(4,1, cv::DataType<double>::type);
//    a1 = rc*Vhc.col(3);
//    std::cout << "a1: " << a1 << std::endl;
//
//    cv::Mat D(4,3, cv::DataType<double>::type);
//
//    D.at<double>(0,0) = C2.at<double>(0);
//    D.at<double>(1,0) = C2.at<double>(1);
//    D.at<double>(2,0) = C2.at<double>(2);
//    D.at<double>(0,1) = a2.at<double>(0);
//    D.at<double>(1,1) = a2.at<double>(1);
//    D.at<double>(2,1) = a2.at<double>(2);
//    D.at<double>(0,2) = a3.at<double>(0);
//    D.at<double>(1,2) = a3.at<double>(1);
//    D.at<double>(2,2) = a3.at<double>(2);
//    D.at<double>(3,0) = 1;
//    D.at<double>(3,1) = 0;
//    D.at<double>(3,2) = 0;
//
//    std::cout << "D: " << D << std::endl;
//    cv::Mat Dl = D.t();
//    std::cout << "D': " << D.t() << std::endl;
//    cv::Mat Ud(3, 3, cv::DataType<double>::type);
//    cv::Mat Sd(1, 4, cv::DataType<double>::type);
//    cv::Mat Vhd(4, 4, cv::DataType<double>::type);
//
//    cv::SVD::compute(Dl , Sd, Ud, Vhd, cv::SVD::FULL_UV );
//
//    std::cout << "Ud : " << Ud << std::endl;
//    std::cout << "Sd : " << Sd << std::endl;
//    std::cout << "Vhd : " << Vhd << std::endl;
//
//    cv::Mat Vhtempd(3,1, cv::DataType<double>::type);
//    Vhtempd.at<double>(0) = Vhd.at<double>(0, 3);
//    Vhtempd.at<double>(1) = Vhd.at<double>(1, 3);
//    Vhtempd.at<double>(2) = Vhd.at<double>(2, 3);
//    double rd = norm(au,cv::NORM_L2)/norm(Vhtempd, cv::NORM_L2);
//    std::cout << "rd: " << rd << std::endl;
//    cv::Mat b1(4,1, cv::DataType<double>::type);
//    b1 = rd*Vhd.col(3);
//    std::cout << "b1: " << b1 << std::endl;
//
//    cv::Mat H(3,3, cv::DataType<double>::type);
//
//    H.at<double>(0,0) = 1;
//    H.at<double>(0,1) = 0;
//    H.at<double>(0,2) = 0;
//    H.at<double>(1,0) = 0;
//    H.at<double>(1,1) = 1;
//    H.at<double>(1,2) = 0;
//    H.at<double>(2,0) = 0;
//    H.at<double>(2,1) = 0;
//    H.at<double>(2,2) = 1;
//
//    cv::Mat Pn1temp(4,3, cv::DataType<double>::type), Pn2temp(4,3, cv::DataType<double>::type);
//    Pn1temp.at<double>(0,0) = a1.at<double>(0);
//    Pn1temp.at<double>(0,1) = a2.at<double>(0);
//    Pn1temp.at<double>(0,2) = a3.at<double>(0);
//    Pn1temp.at<double>(1,0) = a1.at<double>(1);
//    Pn1temp.at<double>(1,1) = a2.at<double>(1);
//    Pn1temp.at<double>(1,2) = a3.at<double>(1);
//    Pn1temp.at<double>(2,0) = a1.at<double>(2);
//    Pn1temp.at<double>(2,1) = a2.at<double>(2);
//    Pn1temp.at<double>(2,2) = a3.at<double>(2);
//    Pn1temp.at<double>(3,0) = a1.at<double>(3);
//    Pn1temp.at<double>(3,1) = a2.at<double>(3);
//    Pn1temp.at<double>(3,2) = a3.at<double>(3);
//
//    Pn2temp.at<double>(0,0) = b1.at<double>(0);
//    Pn2temp.at<double>(0,1) = a2.at<double>(0);
//    Pn2temp.at<double>(0,2) = a3.at<double>(0);
//    Pn2temp.at<double>(1,0) = b1.at<double>(1);
//    Pn2temp.at<double>(1,1) = a2.at<double>(1);
//    Pn2temp.at<double>(1,2) = a3.at<double>(1);
//    Pn2temp.at<double>(2,0) = b1.at<double>(2);
//    Pn2temp.at<double>(2,1) = a2.at<double>(2);
//    Pn2temp.at<double>(2,2) = a3.at<double>(2);
//    Pn2temp.at<double>(3,0) = b1.at<double>(3);
//    Pn2temp.at<double>(3,1) = a2.at<double>(3);
//    Pn2temp.at<double>(3,2) = a3.at<double>(3);
//
//    Pn1 = H*Pn1temp.t();
//    Pn2 = H*Pn2temp.t();
//
//    Qn1.at<double>(0,0) = Pn1.at<double>(0,0);
//    Qn1.at<double>(0,1) = Pn1.at<double>(0,1);
//    Qn1.at<double>(0,2) = Pn1.at<double>(0,2);
//    Qn1.at<double>(1,0) = Pn1.at<double>(1,0);
//    Qn1.at<double>(1,1) = Pn1.at<double>(1,1);
//    Qn1.at<double>(1,2) = Pn1.at<double>(1,2);
//    Qn1.at<double>(2,0) = Pn1.at<double>(2,0);
//    Qn1.at<double>(2,1) = Pn1.at<double>(2,1);
//    Qn1.at<double>(2,2) = Pn1.at<double>(2,2);
//
//    Qn2.at<double>(0,0) = Pn2.at<double>(0,0);
//    Qn2.at<double>(0,1) = Pn2.at<double>(0,1);
//    Qn2.at<double>(0,2) = Pn2.at<double>(0,2);
//    Qn2.at<double>(1,0) = Pn2.at<double>(1,0);
//    Qn2.at<double>(1,1) = Pn2.at<double>(1,1);
//    Qn2.at<double>(1,2) = Pn2.at<double>(1,2);
//    Qn2.at<double>(2,0) = Pn2.at<double>(2,0);
//    Qn2.at<double>(2,1) = Pn2.at<double>(2,1);
//    Qn2.at<double>(2,2) = Pn2.at<double>(2,2);
//
//    std::cout << "Pn1: " << Pn1 << std::endl;
//    std::cout << "Pn2: " << Pn2 << std::endl;
//
//    Transform1f = Qn1*(Qo1.inv());
//    Transform2f = Qn2*(Qo2.inv());
//
//    Transform1r = Qo1*(Qn1.inv());
//    Transform2r = Qo2*(Qn2.inv());
//
//    std::cout << "Transform1: " << Transform1r << std::endl;
//    std::cout << "Transform2: " << Transform2r << std::endl;
//
//    return;
//}


//double DynamNCC(int WinSize, int row, int colLeft, int colRight, cv::Mat leftIm, cv::Mat rightIm){
//    cv::Mat Im1Win(leftIm.rows, leftIm.cols, cv::DataType<double>::type), Im2Win(leftIm.rows, leftIm.cols, cv::DataType<double>::type);
//    double temp1 = 0.0, temp2 = 0.0, mean1 = 0.0, mean2 = 0.0;
//    for(int x = 0; x < 2*WinSize+1; x++){
//        for(int y = 0; y < 2*WinSize+1; y++){
//            Im1Win.at<double>(x,y) = (double)leftIm.at<uchar>(row - WinSize + x, colLeft - WinSize + y);
//            temp1 += (double)leftIm.at<uchar>(row - WinSize + x, colLeft - WinSize + y);
//        }
//    }
//
//    for(int x = 0; x < 2*WinSize+1; x++){
//        for(int y = 0; y < 2*WinSize+1; y++){
//            Im2Win.at<double>(x,y) = (double)rightIm.at<uchar>(row - WinSize + x, colRight - WinSize + y);
//            temp2 += (double)rightIm.at<uchar>(row - WinSize + x, colRight - WinSize + y);
//        }
//    }
//
//    mean1 = temp1/(pow(2*WinSize+1, 2));
//    mean2 = temp2/(pow(2*WinSize+1, 2));
//    //                std::cout << "Mean of First array = " << mean1 << std::endl;
//    //                std::cout << "Mean of Second array = " << mean2 << std::endl;
//
//    double testing = 0.0;
//    testing = (Im1Win.dot(Im2Win) - (pow(2*WinSize+1, 2)*mean1*mean2))/(sqrt(pow(norm(Im1Win), 2) - pow(2*WinSize+1, 2)*pow(mean1, 2))*sqrt(pow(norm(Im2Win), 2) - pow(2*WinSize+1, 2)*pow(mean2, 2)));
//    if(testing > 1 || testing < -1){
//        //                    std::cout << "error" << std::endl;
//        //                    std::cout << "Dot: " << Im1Win.dot(Im2Win) << std::endl;
//        //                    std::cout << "Norm1: " << norm(Im1Win) << std::endl;
//        //                    std::cout << "Norm2: " <<  norm(Im2Win) << std::endl;
//        testing = -1;
//    }
//    //                std::cout << "NCC" << testing << std::endl;
//
//    NCCvalues.push_back(testing);
//
//}
//
//tempmin = 2;
//double tempval = 0;
//for(int xb = 0; xb < NCCvalues.size(); xb ++){
//    if(NCCvalues[xb] < 0){
//        tempval = 1 + abs(NCCvalues[xb]);
//    }
//    else if(NCCvalues[xb] > 1 || NCCvalues[xb] < -1){
//        std::cout << "error" << std::endl;
//        }
//        else{
//            tempval = 1 - NCCvalues[xb];
//
//        }
//        if(tempval < tempmin){
//            tempmin = tempval;
//            index = xb + MinDisp;
//        }
//        }
//
//        double scale = MaxDisp/255;
//        double scaled = (double)index/scale;
//        DispIm.at<double>(row,col) = scaled;
//    return;
//}

double DynamicSAD(int WinSize, int row, int colLeft, int colRight, cv::Mat leftIm, cv::Mat rightIm){
    cv::Mat Im1Win(leftIm.rows, leftIm.cols, cv::DataType<double>::type), Im2Win(leftIm.rows, leftIm.cols, cv::DataType<double>::type), SADMatrix(leftIm.rows, leftIm.cols, cv::DataType<double>::type);
    double Cost = 0.0;
    for(int x = 0; x < 2*WinSize+1; x++){
        for(int y = 0; y < 2*WinSize+1; y++){
            Im1Win.at<double>(x,y) = (double)leftIm.at<uchar>(row - WinSize + x, colLeft - WinSize + y);
        }
    }
    
    for(int x = 0; x < 2*WinSize+1; x++){
        for(int y = 0; y < 2*WinSize+1; y++){
            Im2Win.at<double>(x,y) = (double)rightIm.at<uchar>(row - WinSize + x, colRight - WinSize + y);
        }
    }
    
    SADMatrix = Im1Win - Im2Win;
    
    Cost = 0;
    for(int x = 0; x < 2*WinSize+1; x++){
        for(int y = 0; y < 2*WinSize+1; y++){
            Cost += SADMatrix.at<double>(x,y);
        }
    }
    
    return Cost;
}

void DynamicProg(cv::Mat leftIm, cv::Mat rightIm){
    cv::Mat Cost(leftIm.cols, leftIm.cols, cv::DataType<double>::type, cv::Scalar(0)), dispLeft(leftIm.rows, leftIm.cols, cv::DataType<double>::type), dispRight(leftIm.rows, leftIm.cols, cv::DataType<double>::type), Path(leftIm.cols, leftIm.cols, cv::DataType<double>::type, cv::Scalar(1));
    double occ = 0.00009, temp = 0.0, min1 = 0.0, min2 = 0.0, min3 = 0.0;
    std::cout << "Started Dynamic" << std::endl;
    for (int row = 0; row < leftIm.rows; row ++) {
        for(int i = 1; i < leftIm.cols; i++){
            Cost.at<double>(i, 0) = i*occ;
        }
        for(int j = 1; j < leftIm.cols; j++){
            Cost.at<double>(0, j) = j*occ;
        }
        for(int i = 1; i < leftIm.cols; i ++){
            for(int j = 1; j < leftIm.cols; j++){
//                temp = DynamicSAD(1, row, i, j, leftIm, rightIm);
                temp = pow(((double)leftIm.at<uchar>(row, i) - (double)rightIm.at<uchar>(row, j)), 2);
                min1 = Cost.at<double>(i - 1, j - 1) + temp;
                min2 = Cost.at<double>(i - 1, j) + occ;
                min3 = Cost.at<double>(i, j - 1) + occ;
                if(min1 < min2 && min1 < min3){
                    Cost.at<double>(i, j) = min1;
                    Path.at<double>(i,j) = 1;
                }
                else if(min2 < min1 && min2 < min3){
                    Cost.at<double>(i, j) = min2;
                    Path.at<double>(i,j) = 2;
                }
                else if(min3 < min2 && min3 < min1){
                    Cost.at<double>(i, j) = min3;
                    Path.at<double>(i,j) = 3;
                }
            }
        }
        
        double i = leftIm.cols - 1;
        double j = leftIm.cols - 1;
        std::cout << "Done with Costing" << std::endl;
        
        while(i !=1 && j != 1){
            switch ((int)Path.at<double>(i,j)) {
                case 1:
                    dispLeft.at<double>(row, i) = abs(i - j);
                    dispRight.at<double>(row, j) = abs(j - i);
                    i --;
                    j --;
                    break;
                    
                case 2:
                    dispLeft.at<double>(row, i) = -1;
                    i --;
                    break;
                    
                case 3:
                    dispLeft.at<double>(row, j) = -1;
                    j --;
                    break;
            }
        }
        std::cout << "Dynamic-Row: " << row << std::endl;
        
    }
    cv::imwrite("../Range_Images/DynamicDispL.png", dispLeft);
    cv::imwrite("../Range_Images/DynamicDispR.png", dispRight);
    
    return;
}





//int findMin(cv::Mat T, int max, int col){
//    int index = 0;
//    double temp = 0, value = 0;
//    int start = 0;
//    for(int check = 0; check < max; check ++){
//        temp = T.at<double>(check, col);
//        index ++;
//        start = index;
//        if(temp != 0){
//            break;
//        }
//    }
//
//    for(int i = start; i <= max; i ++){
//        value = T.at<double>(i, col);
//        if(value !=0 && value <= temp){
//            temp = value;
//            index = i;
//        }
//    }
//    return index;
//}
//
//int findMindMin(cv::Mat T, int max, int min, int col){
//    int index = 0;
//    double temp = 0, value = 0;
//    int start = 0;
//    for(int check = 0; check < max - min; check ++){
//        temp = T.at<double>(check, col);
//        if(check != 0){
//            index ++;
//        }
//        start = index;
//        if(temp != 0){
//            break;
//        }
//    }
//
//    for(int i = start; i <= max - min; i ++){
//        value = T.at<double>(i, col);
//        if(value !=0 && value <= temp){
//            temp = value;
//            index = i;
//        }
//    }
//    return index;
//}
//
//void Dynamic(cv::Mat DispIm, int dMax, int WinSize, cv::Mat I1, cv::Mat I2){
//    cv::Mat T(dMax + 1 ,1351, cv::DataType<double>::type), DSI(dMax + 1 ,1351, cv::DataType<double>::type);
//    std::vector <cv::Mat> PreTab;
//    cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrix(2*WinSize+1,2*WinSize+1,  cv::DataType<double>::type), PreTabMat(dMax + 1, 1351, CV_32FC2);
//    int dispIndex = 0;
//    double SADWin = 0.0;
//    std::cout << "Starting Dynamic Programming" << std::endl;
//    for(int row = 0; row <= 1230; row ++){
//        for(int col = 0; col <= 1350; col++){
//            for(int d = 0; d <= dMax; d++){
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col - WinSize);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col - WinSize + 1);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col + WinSize - 1);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col + WinSize);
//
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col - WinSize + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col - WinSize + 1 + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + WinSize - 1 + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + WinSize + d);
//                    }
//                }
//                //                                std::cout << "Im1Win : " <<Im1Win << std::endl;
//                //                                std::cout << "Im2Win : " <<Im2Win << std::endl;
//                SADMatrix = abs(Im1Win - Im2Win);
//                //                std::cout << "SADMat : " <<SADMatrix << std::endl;
//                SADWin = 0;
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        SADWin = SADWin + SADMatrix.at<double>(x,y);
//                    }
//                }
//
//                DSI.at<double>(d , col) = SADWin;
//                //                std::cout << "SADWin : " << SADWin << std::endl;
//                //                if(DSI.at<double>(d, col) < 0.1){
//                //                    DSI.at<double>(d, col) = 0;
//                //                }
//            }//end of disp loop
//        }//end of col loop
//        //        std::cout << "DSI : " << DSI << std::endl;
//
//        for(int initiald = 0; initiald <= dMax; initiald++){
//            //            std::cout << "DSI_T: " << DSI.at<double>(initiald - dMin, 0) << std::endl;
//            T.at<double>(initiald, 0) = DSI.at<double>(initiald, 0);
//            PreTabMat.at<cv::Vec2f>(initiald, 0)[0] = initiald;
//            PreTabMat.at<cv::Vec2f>(initiald, 0)[1] = -1;
//        }
//
//        for(int col = 1; col <= 1350; col++){
//            for(int d = 1; d <= dMax; d++){
//                int x_po = 0, d_po = 0, index = 0;
//                double temp = 0.0, initial = 0.0;
//                for(int Sda = -1; Sda <= 1; Sda++){
//
//                    if(Sda == -1){
//                        initial = T.at<double>( d + Sda, col - 1);
//                    }
//                    temp = T.at<double>( d + Sda, col - 1);
//                    if(temp <= initial && Sda < 1){
//                        index = Sda;
//                        initial = temp;
//                    }
//                    else if (temp < initial && temp > 1){
//                        index = Sda;
//                        initial = temp;
//                    }
//                }
//
//                x_po = col - 1;
//                d_po = d + index;
//                PreTabMat.at<cv::Vec2f>(d , col)[0] = d_po;
//                PreTabMat.at<cv::Vec2f>(d , col)[1] = x_po;
//                //                std::cout << "x_PO: " << x_po << std::endl;
//                //                std::cout << "d_po: " << d_po << std::endl;
//                //                std::cout << "T_PO: " << T.at<double>(d_po, x_po) << std::endl;
//                //                std::cout << "DSI_PO: " << DSI.at<double>(d - dMin, col) << std::endl;
//
//
//                T.at<double>(d , col) = DSI.at<double>(d , col) +  T.at<double>(d_po, x_po);
//
//            }//end of disp loop
//        } // end of col loop
//        //        for(int i = 0; i <= dMax - dMin; i++){
//        //            std::cout << "T Matrix: "<< T.at<double>(i, 640 - 2) << "     " << i << std::endl;
//        //        }
//
//        dispIndex = findMindMin(T, dMax, 0, 1350 - 2 );
//        //        std::cout << "Index: " << dispIndex << std::endl;
//
//        //        for(int x = 0; x <= 640 - 2; x++){
//        //            std::cout << "col" << x << std::endl;
//        //            for(int i = 0; i < dMax - dMin; i++){
//        //            std::cout << "Pretab d: " << PreTabMat.at<cv::Vec2f>(i, x)[0] << "\t" << PreTabMat.at<cv::Vec2f>(i, x)[1] << std::endl;
//        //            }
//        //        }
//
//        int x = 1280 - 2;
//        int xp, dp;
//        while(x >= 0){
//            DispIm.at<double>(row,x) = dispIndex;
//            //            DispIm.at<double>(row,x) = DSI.at<double>(dispIndex, x);
//            xp = PreTabMat.at<cv::Vec2f>(dispIndex, x)[1];
//            dp = PreTabMat.at<cv::Vec2f>(dispIndex, x)[0];
//            x = xp;
//            dispIndex = dp;
//            //            dispIndex = findMindMin(T, dMax, dMin, x);
//        }
//        std::cout << "Row: " << row << std::endl;
//    }
//    cv::imwrite("NTestingDynamic.png", DispIm);
//    return;
//}
//
//void DynamicdMin(cv::Mat DispIm, int dMax, int dMin, int WinSize, cv::Mat I1, cv::Mat I2){
//    cv::Mat T(dMax + 1 - dMin,1281, cv::DataType<double>::type), DSI(dMax + 1 - dMin,1281, cv::DataType<double>::type);
//    std::vector <cv::Mat> PreTab;
//    cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrix(2*WinSize+1,2*WinSize+1,  cv::DataType<double>::type), PreTabMat(dMax + 1 - dMin, 1281, CV_32FC2);
//    int dispIndex = 0;
//    double SADWin = 0.0;
//    std::cout << "Starting Dynamic Programming" << std::endl;
//    for(int row = 0; row <= 960; row ++){
//        for(int col = 0; col <= 1281; col++){
//            for(int d = dMin; d <= dMax; d++){
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col - WinSize);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col - WinSize + 1);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col + WinSize - 1);
//                        Im1Win.at<double>(x,y) = I1.at<double>(row - WinSize + x, col + WinSize);
//
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col - WinSize + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col - WinSize + 1 + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + WinSize - 1 + d);
//                        Im2Win.at<double>(x,y) = I2.at<double>(row - WinSize + x, col + WinSize + d);
//                    }
//                }
//                //                                std::cout << "Im1Win : " <<Im1Win << std::endl;
//                //                                std::cout << "Im2Win : " <<Im2Win << std::endl;
//                SADMatrix = abs(Im1Win - Im2Win);
//                //                std::cout << "SADMat : " <<SADMatrix << std::endl;
//                SADWin = 0;
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        SADWin = SADWin + SADMatrix.at<double>(x,y);
//                    }
//                }
//
//                DSI.at<double>(d - dMin, col) = SADWin;
//                //                std::cout << "SADWin : " << SADWin << std::endl;
//                //                if(DSI.at<double>(d, col) < 0.1){
//                //                    DSI.at<double>(d, col) = 0;
//                //                }
//            }//end of disp loop
//        }//end of col loop
//        //        std::cout << "DSI : " << DSI << std::endl;
//
//        for(int initiald = dMin; initiald <= dMax; initiald++){
//            //            std::cout << "DSI_T: " << DSI.at<double>(initiald - dMin, 0) << std::endl;
//            T.at<double>(initiald - dMin, 0) = DSI.at<double>(initiald - dMin, 0);
//            PreTabMat.at<cv::Vec2f>(initiald - dMin,0)[0] = initiald - dMin;
//            PreTabMat.at<cv::Vec2f>(initiald - dMin,0)[1] = -1;
//        }
//
//        for(int col = 1; col <= 1281; col++){
//            for(int d = dMin + 1; d <= dMax; d++){
//                int x_po = 0, d_po = 0, index = 0;
//                double temp = 0.0, initial = 0.0;
//                for(int Sda = -1; Sda <= 1; Sda++){
//
//                    if(Sda == -1){
//                        initial = T.at<double>( d - dMin + Sda, col - 1);
//                    }
//                    temp = T.at<double>( d - dMin + Sda, col - 1);
//                    if(temp <= initial && Sda < 1){
//                        index = Sda;
//                        initial = temp;
//                    }
//                    else if (temp < initial && temp > 1){
//                        index = Sda;
//                        initial = temp;
//                    }
//                }
//
//                x_po = col - 1;
//                d_po = d - dMin + index;
//                PreTabMat.at<cv::Vec2f>(d - dMin, col)[0] = d_po;
//                PreTabMat.at<cv::Vec2f>(d - dMin, col)[1] = x_po;
//                //                std::cout << "x_PO: " << x_po << std::endl;
//                //                std::cout << "d_po: " << d_po << std::endl;
//                //                std::cout << "T_PO: " << T.at<double>(d_po, x_po) << std::endl;
//                //                std::cout << "DSI_PO: " << DSI.at<double>(d - dMin, col) << std::endl;
//
//
//                T.at<double>(d - dMin, col) = DSI.at<double>(d - dMin, col) +  T.at<double>(d_po, x_po);
//
//            }//end of disp loop
//        } // end of col loop
//        //        for(int i = 0; i <= dMax - dMin; i++){
//        //            std::cout << "T Matrix: "<< T.at<double>(i, 640 - 2) << "     " << i << std::endl;
//        //        }
//
//        dispIndex = findMindMin(T, dMax, dMin, 1280 - 2 );
//        //        std::cout << "Index: " << dispIndex << std::endl;
//
//        //        for(int x = 0; x <= 640 - 2; x++){
//        //            std::cout << "col" << x << std::endl;
//        //            for(int i = 0; i < dMax - dMin; i++){
//        //            std::cout << "Pretab d: " << PreTabMat.at<cv::Vec2f>(i, x)[0] << "\t" << PreTabMat.at<cv::Vec2f>(i, x)[1] << std::endl;
//        //            }
//        //        }
//
//        int x = 1280 - 2;
//        int xp, dp;
//        while(x >= 0){
//            DispIm.at<double>(row,x) = dispIndex + dMin;
//            //            DispIm.at<double>(row,x) = DSI.at<double>(dispIndex, x);
//            xp = PreTabMat.at<cv::Vec2f>(dispIndex, x)[1];
//            dp = PreTabMat.at<cv::Vec2f>(dispIndex, x)[0];
//            x = xp;
//            dispIndex = dp;
//            //            dispIndex = findMindMin(T, dMax, dMin, x);
//        }
//        std::cout << "Row: " << row << std::endl;
//    }
//    cv::imwrite("NTestingDynamicMin.png", DispIm);
//    return;
//}

#endif /* RangeExtraction_h */


//void NCC(int WinSize, int MinDisp, int MaxDisp, cv::Mat I1, cv::Mat I2, int Blur, cv::Mat &DispIm){
//    double temp1 = 0, temp2 = 0, mean1, mean2, tempmin;
//    int rowIndex = 0, Offset = 0, index = 0;
//    cv::Mat gray_imageI1(1230, 1350, CV_8U), gray_imageI2(1230, 1350, CV_8U), cleanDisp(1230, 1350, cv::DataType<double>::type);
//    std::vector<cv::Mat> Im2Mat;
//    std::vector<double> NCCvalues;
//
//    cv::cvtColor(I1, gray_imageI1, cv::COLOR_BGR2GRAY);
//    cv::cvtColor(I2, gray_imageI2, cv::COLOR_BGR2GRAY);
//
//    cv::Mat Kernel(3,3, cv::DataType<double>::type);
//    double factor = 1.0;
//    Kernel.at<double>(0,0) = 1;
//    Kernel.at<double>(0,1) = 2;
//    Kernel.at<double>(0,2) = 1;
//    Kernel.at<double>(1,0) = 2;
//    Kernel.at<double>(1,1) = 4;
//    Kernel.at<double>(1,2) = 2;
//    Kernel.at<double>(2,0) = 1;
//    Kernel.at<double>(2,1) = 2;
//    Kernel.at<double>(2,2) = 1;
//    factor = 1.0/16.0;
//
//    cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
//
//    for(int row = WinSize; row < 1230 - WinSize; row++){
//        for(int col = WinSize; col < 1350 - WinSize; col++){
//            rowIndex = row + Offset;
//            NCCvalues.clear();
//            temp1 = 0.0;
//            for(int x = 0; x < 2*WinSize+1; x++){
//                for(int y = 0; y < 2*WinSize+1; y++){
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col - WinSize + y);
//                    temp1 += (double)gray_imageI1.at<uchar>(row - WinSize + x, col - WinSize + y);
//                }
//            }
//            //            std::cout << "Im1 = " << Im1Win << std::endl;
//            if(temp1 == 0){
//                DispIm.at<double>(row,col) = 0.0;
//                continue;
//            }
//
//            for(int i = MinDisp; i < MaxDisp; i++){
//                temp2 = 0.0;
//                mean1 = 0.0;
//                mean2 = 0.0;
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(row - WinSize + x, col - WinSize + y + i);
//                        temp2 += (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col - WinSize + y + i);
//                    }
//                }
//                //                std::cout << "Im2 = " << Im2Win << std::endl;
//                mean1 = temp1/(pow(2*WinSize+1, 2));
//                mean2 = temp2/(pow(2*WinSize+1, 2));
//                //                std::cout << "Mean of First array = " << mean1 << std::endl;
//                //                std::cout << "Mean of Second array = " << mean2 << std::endl;
//
//                if(temp2 == 0){
//                    NCCvalues.push_back(-1);
//                    continue;
//                }
//                double testing = 0.0;
//                testing = (Im1Win.dot(Im2Win) - (pow(2*WinSize+1, 2)*mean1*mean2))/(sqrt(pow(norm(Im1Win), 2) - pow(2*WinSize+1, 2)*pow(mean1, 2))*sqrt(pow(norm(Im2Win), 2) - pow(2*WinSize+1, 2)*pow(mean2, 2)));
//                if(testing > 1 || testing < -1){
//                    //                    std::cout << "error" << std::endl;
//                    //                    std::cout << "Dot: " << Im1Win.dot(Im2Win) << std::endl;
//                    //                    std::cout << "Norm1: " << norm(Im1Win) << std::endl;
//                    //                    std::cout << "Norm2: " <<  norm(Im2Win) << std::endl;
//                    testing = -1;
//                }
//                //                std::cout << "NCC" << testing << std::endl;
//
//                NCCvalues.push_back(testing);
//
//            }
//
//            tempmin = 2;
//            double tempval = 0;
//            for(int xb = 0; xb < NCCvalues.size(); xb ++){
//                if(NCCvalues[xb] < 0){
//                    tempval = 1 + abs(NCCvalues[xb]);
//                }
//                else if(NCCvalues[xb] > 1 || NCCvalues[xb] < -1){
//                    std::cout << "error" << std::endl;
//                }
//                else{
//                    tempval = 1 - NCCvalues[xb];
//
//                }
//                if(tempval < tempmin){
//                    tempmin = tempval;
//                    index = xb + MinDisp;
//                }
//            }
//
//            double scale = MaxDisp/255;
//            double scaled = (double)index/scale;
//            DispIm.at<double>(row,col) = scaled;
//
//        }
//        std::cout << "Row: " << row << std::endl;
//    }
//
//    for(int x = 0; x < DispIm.cols; x++){
//        for(int y = 0; y < DispIm.rows; y++){
//            double grayValue = 0.0;
//
//            for(int filterY = 0; filterY < 3; filterY++){
//                for(int filterX = 0; filterX < 3; filterX++){
//                    int imageX = (x - 3 / 2 + filterX + DispIm.cols) % DispIm.cols;
//                    int imageY = (y - 3 / 2 + filterY + DispIm.rows) % DispIm.rows;
//                    grayValue += DispIm.at<double>(imageY, imageX)*Kernel.at<double>(filterY,filterX);
//                    //                    std::cout << "TempDisp: " << DispIm.at<double>(imageY, imageX) << std::endl;
//                    //                    std::cout << "Kernel: " << Kernel.at<double>(filterY,filterX) << std::endl;
//                }
//            }
//            DispIm.at<double>(y, x) = fmin(fmax(int(factor*grayValue), 0), 255);
//        }
//    }
//
//    if(Blur == 0){
//        cv::imwrite("../Range_Images/DispIm/NCCLeft_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I1);
//        cv::imwrite("../Range_Images/DispIm/NCCRight_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I2);
//        cv::imwrite("../Range_Images/DispIm/NCCDispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", DispIm);
//        std::cout << "finished Disparity Map" << std::endl;
//    }
//    else if(Blur == 1){
//        cv::imwrite("../Range_Images/DispIm/NCCLeft_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I1);
//        cv::imwrite("../Range_Images/DispIm/NCCRight_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I2);
//        cv::imwrite("../Range_Images/DispIm/NCCDispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", DispIm);
//        std::cout << "finished NCC Disparity Map" << std::endl;
//        std::cout << "DispIm: " << DispIm << std::endl;
//    }
//
//    return;
//}





//void inverseSAD(int WinSize, int MinDisp, int MaxDisp, cv::Mat I1, cv::Mat I2, int sharp, cv::Mat &DispIm){
//    int Offset = 0, index = 0;
//    double SADWin = 0.0, temp = 0.0, Im1Avg = 0.0;
//    int rowIndex = 0;
//    cv::Mat gray_imageI1(1230, 1350, CV_8U), gray_imageI2(1230, 1350, CV_8U);
//    std::vector<cv::Mat> Im2Mat;
//    std::vector<double> SADvalues;
//    std::cout << "Starting Disp map." << std::endl;
//    //    std::cout << I1.type() << std::endl;
//    cv::cvtColor(I1, gray_imageI1, cv::COLOR_BGR2GRAY);
//    cv::cvtColor(I2, gray_imageI2, cv::COLOR_BGR2GRAY);
//
//    for(int row = WinSize; row < 1230 - WinSize; row++){
//        for(int col = WinSize; col < 1350 - WinSize; col++){
//            rowIndex = row + Offset;
//            SADvalues.clear();
//
//            cv::Mat Im1Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2Win(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrix(2*WinSize+1,2*WinSize+1,  cv::DataType<double>::type);
//
//            for(int x = 0; x < 2*WinSize+1; x++){
//                for(int y = 0; y < 2*WinSize+1; y++){
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col - WinSize);
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col - WinSize + 1);
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col);
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col + WinSize - 1);
//                    Im1Win.at<double>(x,y) = (double)gray_imageI1.at<uchar>(row - WinSize + x, col + WinSize);
//                }
//            }
//            Im1Avg = Average(Im1Win);
//            //            std::cout << "Window" << Im1Win << std::endl;
//            if(Im1Avg == 0){
//                DispIm.at<double>(row,col) = 0;
//                continue;
//            }
//
//            for(int i = MinDisp; i < MaxDisp; i++){
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col - WinSize - i);
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col - WinSize + 1 - i);
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col - i);
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col + WinSize - 1 - i);
//                        Im2Win.at<double>(x,y) = (double)gray_imageI2.at<uchar>(rowIndex - WinSize + x, col + WinSize - i);
//                    }
//                }
//                //                cout << Im2Win << endl;
//                SADMatrix = abs(Im1Win - Im2Win);
//                //                cout << SADMatrix << endl;
//                SADWin = 0;
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        SADWin = SADWin + SADMatrix.at<double>(x,y);
//                        //                        cout << SADWin << endl;
//                    }
//                }
//
//                SADvalues.push_back(SADWin);
//
//            }
//
//            temp = SADvalues[0];
//
//            for(int i = 0; i < SADvalues.size(); i ++){
//                if (SADvalues[i] <= temp ) {
//                    temp = SADvalues[i];
//                    index = i + MinDisp;
//                }
//            }
//            double scale = MaxDisp/255.0;
//            index = index/scale;
//            DispIm.at<double>(row,col) = index;
//        }
//        std::cout << "Row: " << row << std::endl;
//    }
//    cv::imwrite("../Range_Images/NSharp/InvDispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", DispIm);
//    return;
//}
//
//void WTA(cv::Mat DispIm, cv::Mat InvDispIm, cv::Mat &newDisp){
//    cv::Mat Index(1230,1350, cv::DataType<double>::type);
//    for(int row = 0; row < 1230; row++){
//        for(int col = 0; col < 1350; col++){
//            if(DispIm.at<double>(row,col) == InvDispIm.at<double>(row,col)){
//                newDisp.at<double>(row,col) = DispIm.at<double>(row,col);
//                Index.at<double>(row,col) = 1;
//            }
//            else{
//                newDisp.at<double>(row,col) = 0;
//                Index.at<double>(row,col) = 0;
//            }
//        }
//    }
//    for(int row = 0; row < 1230; row++){
//        for(int col = 0; col < 1350; col++){
//            if(Index.at<double>(row,col) == 0){
//                for(int i = 0; i < 400; i++){
//
//                    if (Index.at<double>(row,col - i) == 1) {
//                        newDisp.at<double>(row,col) = DispIm.at<double>(row,col - i);
//                        break;
//                    }
//                    //                    if(Index.at<double>(row - i,col) == 1){
//                    //                        newDisp.at<double>(row,col) = DispIm.at<double>(row - i,col);
//                    //                        break;
//                    //                    }
//                    if (Index.at<double>(row,col + i) == 1) {
//                        newDisp.at<double>(row,col) = DispIm.at<double>(row,col + i);
//                        break;
//                    }
//                    //                    if(Index.at<double>(row + i,col) == 1) {
//                    //                        newDisp.at<double>(row,col) = DispIm.at<double>(row + i,col);
//                    //                        break;
//                    //                    }
//
//                }
//            }
//        }
//    }
//}



//
//
//void SAD(int WinSize, int MinDisp, int MaxDisp, cv::Mat I1, cv::Mat I2, int Blur, cv::Mat &DispIm){
//    int Offset = 0, index = 0;
//    double SADWin = 0.0, temp = 0.0, Im1Avg = 0.0;
//    int rowIndex = 0;
//    cv::Mat leftIm(1230, 1350, CV_8UC3), rightIm(1230, 1350, CV_8UC3);
//    std::vector<cv::Mat> Im2Mat;
//    std::vector<double> SADvalues;
//    std::cout << "Starting SAD Disp map." << std::endl;
//    //    std::cout << I1.type() << std::endl;
//    if(Blur == 0){
//        I1.copyTo(leftIm);
//        I2.copyTo(rightIm);
//    }
//    else if(Blur == 1){
//        SharpeningFilter("Gaus", 0, I1, leftIm);
//        SharpeningFilter("Gaus", 1, I2, rightIm);
//    }
//
//    for(int row = WinSize; row < 1230 - WinSize; row++){
//        for(int col = WinSize; col < 1350 - WinSize; col++){
//            rowIndex = row + Offset;
//            SADvalues.clear();
//
//            cv::Mat Im1WinBlue(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im1WinGreen(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im1WinRed(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2WinBlue(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2WinGreen(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), Im2WinRed(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrixBlue(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type), SADMatrixGreen(2*WinSize+1,2*WinSize+1,  cv::DataType<double>::type), SADMatrixRed(2*WinSize+1,2*WinSize+1, cv::DataType<double>::type);
//
//            for(int x = 0; x < 2*WinSize+1; x++){
//                for(int y = 0; y < 2*WinSize+1; y++){
//
//                    Im1WinBlue.at<double>(x,y) = (double)leftIm.at<cv::Vec3b>(row - WinSize + x, col - WinSize + y)[0];
//
//                    Im1WinGreen.at<double>(x,y) = (double)leftIm.at<cv::Vec3b>(row - WinSize + x, col - WinSize + y)[1];
//
//                    Im1WinRed.at<double>(x,y) = (double)leftIm.at<cv::Vec3b>(row - WinSize + x, col - WinSize + y)[2];
//
//                }
//            }
//            Im1Avg = Average(Im1WinBlue);
//
//            if(Im1Avg == 0){
//                DispIm.at<double>(row,col) = 0.0;
//                continue;
//            }
//            //            std::cout << "WindowB" << Im1WinBlue << std::endl;
//            //            std::cout << "WindowG" << Im1WinGreen << std::endl;
//            //            std::cout << "WindowR" << Im1WinRed << std::endl;
//            for(int i = MinDisp; i < MaxDisp; i++){
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//
//                        Im2WinBlue.at<double>(x,y) = (double)rightIm.at<cv::Vec3b>(rowIndex - WinSize + x, col - WinSize + y + i)[0];
//
//                        Im2WinGreen.at<double>(x,y) = (double)rightIm.at<cv::Vec3b>(rowIndex - WinSize + x, col - WinSize + y + i)[1];
//
//                        Im2WinRed.at<double>(x,y) = (double)rightIm.at<cv::Vec3b>(rowIndex - WinSize + x, col - WinSize + y + i)[2];
//
//                    }
//                }
//                //                cout << Im2Win << endl;
//                SADMatrixBlue = abs(Im1WinBlue - Im2WinBlue);
//                SADMatrixGreen = abs(Im1WinGreen - Im2WinGreen);
//                SADMatrixRed = abs(Im1WinRed - Im2WinRed);
//
//                SADWin = 0;
//                for(int x = 0; x < 2*WinSize+1; x++){
//                    for(int y = 0; y < 2*WinSize+1; y++){
//                        SADWin += SADMatrixBlue.at<double>(x,y) + SADMatrixGreen.at<double>(x,y) + SADMatrixRed.at<double>(x,y);
//                    }
//                }
//
//                SADvalues.push_back(SADWin);
//
//            }
//
//            temp = SADvalues[0];
//
//            for(int x = 0; x < SADvalues.size(); x ++){
//                if (SADvalues[x] <= temp ) {
//                    temp = SADvalues[x];
//                    index = x + MinDisp;
//                }
//            }
//            double scale = MaxDisp/255;
//            double scaled = (double)index/scale;
//            DispIm.at<double>(row,col) = scaled;
//
//            //            if (col == 327 && row == 852) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //                cv::imwrite("Check1.png", I1);
//            //                cv::imwrite("Check2.png", I2);
//            //            }
//            //            if (col == 373 && row == 845) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //                cv::imwrite("Check1.png", I1);
//            //                cv::imwrite("Check2.png", I2);
//            //            }
//            //            if (col == 418 && row == 838) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //                cv::imwrite("Check1.png", I1);
//            //                cv::imwrite("Check2.png", I2);
//            //            }
//            //            if (col == 464 && row == 831) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //                cv::imwrite("Check1.png", I1);
//            //                cv::imwrite("Check2.png", I2);
//            //            }
//            //            if (col == 510 && row == 823) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //            }
//            //            if (col == 556 && row == 816) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + Offset - 3);
//            //                cv::Point Two2(col + index + 3,row + Offset + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //            }
//            //            if (col == 219 && row == 284) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + 2 - 3);
//            //                cv::Point Two2(col + index + 3,row + 2 + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //            }
//            //            if (col == 189 && row == 290) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + 2 - 3);
//            //                cv::Point Two2(col + index + 3,row + 2 + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //            }
//            //            if (col == 158 && row == 295) {
//            //                cv::Point One1(col - 3,row- 3);
//            //                cv::Point Two1(col + 3,row+ 3);
//            //                cv::Point One2(col + index - 3 ,row + 2 - 3);
//            //                cv::Point Two2(col + index + 3,row + 2 + 3);
//            //                cv::rectangle(I1, One1, Two1, 0, 2);
//            //                cv::rectangle(I2, One2, Two2, 0, 2);
//            //            }
//
//        }
//        std::cout << "Row: " << row << std::endl;
//    }
//
//    if(Blur == 0){
//        cv::imwrite("../Range_Images/DispIm/Left_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I1);
//        cv::imwrite("../Range_Images/DispIm/Right_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", I2);
//        cv::imwrite("../Range_Images/DispIm/DispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + ".png", DispIm);
//        std::cout << "finished NCC Disparity Map" << std::endl;
//    }
//
//    else if(Blur == 1)
//        cv::imwrite("../Range_Images/DispIm/Left_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I1);
//    cv::imwrite("../Range_Images/DispIm/Right_Correlated_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", I2);
//    cv::imwrite("../Range_Images/DispIm/DispImage_MinDisp" + std::to_string(MinDisp) + "_MaxDisp" + std::to_string(MaxDisp) + "_Win" + std::to_string(WinSize) + "_Gaus.png", DispIm);
//    std::cout << "finished SAD Blurred Disparity Map" << std::endl;
//    std::cout << "DispIm: " << DispIm << std::endl;
//
//    return;
//}
