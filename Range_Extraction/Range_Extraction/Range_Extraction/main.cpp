#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <cminpack.h>
#include "RangeExtraction.h"

//Sharpening filter size. not currently used
#define filterWidth 3
#define filterHeight 3
//width around center pixel that is used for the NCC function for pixel matching
#define Window 1
//Minimum allowed disparity
#define MinDisp 0
//Maximum allowed disparity
#define MaxDisp 500

//Possible rectified image size
#define recRow 1500
#define recCol 1500

//actual image size
#define frecRow 960
#define frecCol 1280

int main(int argc, char const **argv){
    cv::Mat leftsharpened, rightsharpened, DispIm(recRow,recCol, cv::DataType<double>::type), InvDispIm(recRow,recCol, cv::DataType<double>::type), WTADisp(recRow,recCol, cv::DataType<double>::type), DynamDisp(recRow,recCol, cv::DataType<double>::type);
    cv::Mat left_image, left_gray, right_image, right_gray;
    cv::Mat K1, K2, D1, D2, R, T, T1, T2, R1, R2, P1, P2, Q, C1, C2, Disparity, Transform1, Transform2;
    
    //Initial values which are replaced by the minimum and max pixel value that the inverse mapping function which does the rectification can refer to.
    double xOffset1 = 2000, yOffset1 = 2000, xOffset2 = 2000, yOffset2 = 2000, yMax1 = -2000, yMax2 = -2000, xMax1 = -2000, xMax2 = -2000;
    //checkerboard square size
    double square_size;
    //number of internal corners of the checkerboard, number of images taken, image size in pixels
    int board_width, board_height, num_imgs, imagecols, imagerows;
    //Directories to the images which must be rectified and a disparity map generated.
    std::string left_image_dir = "../Pictures_V3/left71.jpg";
    std::string right_image_dir = "../Pictures_V3/right71.jpg";
    
    //Read the file created by the calibration program with all the calculated information
    //1 refers to the left camera, 2 refers to the right camera, K1,2 is the intrinsic calibration matrix, R1,2 is the rotation matrix of each camera, T1,2 is the translation of the two cameras, R and T is the realtive rotation and translation of the right camera with respect to the left, C1,2 is the camera centers in the world plane, P1,2 is the projection matrices of the cameras before rectification.
    ReadFiles(K1, K2, R1, R2, T1, T2, R, T, C1, C2, P1, P2, board_width, board_height, square_size, num_imgs, imagecols, imagerows);
    
    //This calculates the new projection matrix for each camera in order to rectify the images so that the epipolar lines of the left and right image are parralel to the horizontal plane and all matching points between the two images line on the same horizontal line.
    RectifyingTransform(P1, P2, R1, R2, R, T, C1, C2, K1, K2, Transform1, Transform2);
    
    //Multiplies the rectification transform calculated above with all possible pixel values in order to find the minimum and maximum number of pixels needed to fit the rectified images
    Adjust(Transform1, xOffset1, yOffset1, xMax1, yMax1, imagecols, imagerows);
    Adjust(Transform2, xOffset2, yOffset2, xMax2, yMax2, imagecols, imagerows);
    
    //imports the desired images to be rectified and a disparity map generated.
    LoadImagePoints(left_image_dir, left_image, left_gray);
    LoadImagePoints(right_image_dir, right_image, right_gray);
    
    //Harris corner detector
    Harris(left_gray);
    
    //Calculating the boundry values for the rectifed images from the Adjust function
    double minRow, minCol, maxRow, maxCol;
    int new_width, new_height;
    
    if(xOffset1 < xOffset2){
        minRow = xOffset1;
    }
    else{
        minRow = xOffset2;
    }
    
    if(yOffset1 < yOffset2){
        minCol = yOffset1;
    }
    else{
        minCol = yOffset2;
    }
    
    if(xMax1 < xMax2){
        maxRow = xMax2;
    }
    else{
        maxRow = xMax1;
    }
    
    if(yMax1 < yMax2){
        maxCol = yMax2;
    }
    else{
        maxCol = yMax1;
    }
    new_height = ceil(maxRow) - floor(minRow);
    new_width = ceil(maxCol) - floor(minCol);
    
    cv::Mat Left_Rectified(new_height,new_width, CV_8UC3);
    cv::Mat Right_Rectified(new_height,new_width, CV_8UC3);
    cv::Mat Right_Rectified_Shifted(new_height,new_width, CV_8UC3);

    //Using inverse mapping the rectified images are made
    inverseMapping(Transform1, Transform2, left_image, right_image, Left_Rectified, Right_Rectified, minRow, minCol, new_width, new_height);
    
    //For some unknown reason the images are rectified but there is an offset between the two images. By moving the right image down 130 pixels all the corresponding points lie on the same horizontal lines.
    for(int row = 0; row < Left_Rectified.rows - 130; row ++){
        for(int col = 0; col < Left_Rectified.cols; col ++){
            Right_Rectified_Shifted.at<cv::Vec3b>(row + 130, col)[0] = Left_Rectified.at<cv::Vec3b>(row, col)[0];
            Right_Rectified_Shifted.at<cv::Vec3b>(row + 130, col)[1] = Left_Rectified.at<cv::Vec3b>(row, col)[1];
            Right_Rectified_Shifted.at<cv::Vec3b>(row + 130, col)[2] = Left_Rectified.at<cv::Vec3b>(row, col)[2];
        }
    }
    cv::imwrite("../Range_Images/Right_Rectified.png", Right_Rectified_Shifted);

    cv::Mat LeftHSV(Right_Rectified_Shifted.rows, Right_Rectified_Shifted.cols, CV_8UC3), RightHSV(Right_Rectified_Shifted.rows, Right_Rectified_Shifted.cols, CV_8UC3);
    //Image is converted to HSV format to allow for easy removal of the green background
    cv::cvtColor(Left_Rectified, LeftHSV, cv::COLOR_BGR2HSV);
    cv::cvtColor(Right_Rectified_Shifted, RightHSV, cv::COLOR_BGR2HSV);
    
    //The Background is removed by setting the intensity value to zero where the HSV image indactes the colour is green
    for(int row = 0; row < Left_Rectified.rows; row ++){
        for(int col = 0; col < Left_Rectified.cols; col ++){
            if((double)LeftHSV.at<cv::Vec3b>(row, col)[0] > 35 && (double)LeftHSV.at<cv::Vec3b>(row, col)[0] < 90){
                Left_Rectified.at<cv::Vec3b>(row, col)[0] = 0;
                Left_Rectified.at<cv::Vec3b>(row, col)[1] = 0;
                Left_Rectified.at<cv::Vec3b>(row, col)[2] = 0;
            }
            if((double)RightHSV.at<cv::Vec3b>(row, col)[0] > 35 && (double)RightHSV.at<cv::Vec3b>(row, col)[0] < 90){
                Right_Rectified_Shifted.at<cv::Vec3b>(row, col)[0] = 0;
                Right_Rectified_Shifted.at<cv::Vec3b>(row, col)[1] = 0;
                Right_Rectified_Shifted.at<cv::Vec3b>(row, col)[2] = 0;
            }
        }
    }
    cv::imwrite("../Range_Images/LeftHSV.png", Left_Rectified);
    cv::imwrite("../Range_Images/RightHSV.png", Right_Rectified_Shifted);
    
    cv::Mat Left_Rectified_Gray(Left_Rectified.rows, Left_Rectified.cols, CV_8U), Right_Rectified_Gray(Left_Rectified.rows, Left_Rectified.cols, CV_8U);
    
    cv::cvtColor(Left_Rectified, Left_Rectified_Gray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(Right_Rectified_Shifted, Right_Rectified_Gray, cv::COLOR_BGR2GRAY);
    
    cv::Mat LeftRecGraySobelMag(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), LeftRecGraySobelDir(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), RightRecGraySobelMag(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), RightRecGraySobelDir(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type);
    
    //passes the images though a sobel edge detector. I thought this would help the matching but it did not
//    Filter(Left_Rectified_Gray, LeftRecGraySobelMag, LeftRecGraySobelDir, "Sobel", "Gray");
//    Filter(Right_Rectified_Gray, RightRecGraySobelMag, RightRecGraySobelDir, "Sobel", "Gray");
//    cv::imwrite("../Range_Images/TestSobL.png", LeftRecGraySobelMag);
//    cv::imwrite("../Range_Images/TestSobR.png", RightRecGraySobelMag);
    
    cv::Mat DispImN(Left_Rectified_Gray.rows,Left_Rectified_Gray.cols, cv::DataType<double>::type);
    // The disparity map is generated using the NCC windowing method
    NCC(Window, MinDisp, MaxDisp, Left_Rectified_Gray, Right_Rectified_Gray, 0, DispImN);
    
    //The remainder fo the code is the failed attempts at adding detail to the images where there was very little
    
//    Harris(Left_Rectified_Gray);
//    cv::Mat LeftWolf(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), RightWolf(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type);
//    cv::Mat LeftWolfchar(Left_Rectified.rows, Left_Rectified.cols, CV_8U), RightWolfchar(Left_Rectified.rows, Left_Rectified.cols, CV_8U);

//    shadowWolf(10, 0, 50, 5 , Left_Rectified_Gray, LeftWolfchar);// WIndow(5 - 10), drange(0- 60), var(10 - 100), speed(10-25% std > 4)
//    shadowWolf(10, 0, 50, 5 , Right_Rectified_Gray, RightWolfchar);// WIndow, drange, var, speed
//    cv::imwrite("../Range_Images/ShadowWolfL.png", LeftWolfchar);
//    cv::imwrite("../Range_Images/ShadowWolfR.png", RightWolfchar);
//
//    cv::Mat LeftTextured(Left_Rectified.rows, Left_Rectified.cols, CV_8U),  RightTextured(Left_Rectified.rows,  Left_Rectified.cols, CV_8U);
//    LBP(1, Left_Rectified_Gray, LeftTextured);
//    LBP(1, Right_Rectified_Gray, RightTextured);
//    cv::imwrite("../Range_Images/LeftTextured.png", LeftTextured);
//    cv::imwrite("../Range_Images/RightTextured.png", RightTextured);
//
//    cv::Mat LeftRecGrayUse(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), RightRecGrayUse(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type);
//    cv::Mat LeftRecGrayScaled(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type), RightRecGrayScaled(Left_Rectified.rows, Left_Rectified.cols, cv::DataType<double>::type);
//
//    for(int row = 0; row < Left_Rectified.rows; row ++){
//        for(int col = 0; col < Left_Rectified.cols; col ++){
//            LeftRecGrayUse.at<double>(row, col) = (double)LeftTextured.at<uchar>(row,col) + (double)Left_Rectified_Gray.at<uchar>(row,col);
//            RightRecGrayUse.at<double>(row, col) = (double)RightTextured.at<uchar>(row,col) + (double)Right_Rectified_Gray.at<uchar>(row,col);
//        }
//    }
//    cv::imwrite("../Range_Images/LeftTexturedUnscale.png", LeftRecGrayUse);
//    cv::imwrite("../Range_Images/RightTexturedUnscale.png", RightRecGrayUse);
//    double maxLeft = findMax(LeftRecGrayUse);
//    double maxRight = findMax(RightRecGrayUse);
//    double scaleL = maxLeft/255.0;
//    double scaleR = maxRight/255.0;
//
//    for(int row = 0; row < Left_Rectified.rows; row ++){
//        for(int col = 0; col < Left_Rectified.cols; col ++){
//            LeftRecGrayScaled.at<double>(row, col) = LeftRecGrayUse.at<double>(row, col)/scaleL;
//            RightRecGrayScaled.at<double>(row, col) = RightRecGrayUse.at<double>(row, col)/scaleR;
//        }
//    }
//    cv::imwrite("../Range_Images/LeftTexturedScale.png", LeftRecGrayScaled);
//    cv::imwrite("../Range_Images/RightTexturedScale.png", RightRecGrayScaled);
//    shadowWolf(10, 0, 50, 5 , Left_Rectified_Gray, LeftWolf);// WIndow, drange, var, speed
//    shadowWolf(10, 0, 50, 5 , Right_Rectified_Gray, RightWolf);// WIndow, drange, var, speed
//    cv::Mat imageR = cv::imread("../Pictures/aloeL.jpg");
//    cv::Mat imageL = cv::imread("../Pictures/aloeR.jpg");
//    cv::Mat LGray(imageL.rows, imageL.cols, CV_8U), RGray(imageR.rows, imageR.cols, CV_8U);
//    cv::cvtColor(imageL, LGray, cv::COLOR_BGR2GRAY);
//    cv::cvtColor(imageR, RGray, cv::COLOR_BGR2GRAY);
//    cv::Mat DispImN(Left_Rectified_Gray.rows,Left_Rectified_Gray.cols, cv::DataType<double>::type);
//    NCC(Window, MinDisp, MaxDisp, Left_Rectified_Gray, Right_Rectified_Gray, 0, DispImN);
//    SAD(Window, MinDisp, MaxDisp, LeftRecGrayScaled, RightRecGrayScaled, 0, DispIm);
    
//    DynamicProg(Left_Rectified_Gray, Right_Rectified_Gray);
//    inverseSAD(Window, MinDisp, MaxDisp, Right_Rectified_Shifted, Left_Rectified, 2, InvDispIm);
//
//    WTA(DispIm, InvDispIm, WTADisp);
//
//    cv::imwrite("WTADisp.png", WTADisp);
    
    //    Dynamic(DynamDisp, MaxDisp, Window, Left_Rectified, Right_Rectified_Shifted);
    //
    //    DynamicdMin(DynamDisp, MaxDisp, MinDisp, Window, Left_Sharpened, Right_Sharpened);
    //
    
    return 0;
}
