//
//  IntrinsicCalibration.hpp
//  Calibration
//
//  Created by Mathew Strydom on 2019/08/20.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#ifndef IntrinsicCalibration_h
#define IntrinsicCalibration_h

#include <stdio.h>
#include <iostream>
#include <cmath>
//#include <cminpack.h>

// Vector storing the chessboard corners 2D coordinates. reset for each image
std::vector< cv::Point2f > corners;

//File directory to store images with drawn detected chessboard corners
std::string Debug_Dir = "Chessboard_Detect_Debug/";
std::string Sobel_Dir = "../Sobel/";
std::string Blur_Dir = "../Blurred/";

//Filter function capable of performing a Guassian filter or Sobel Edge detection
//I initially used the bluring to try counteract the jpeg compression effects on the images. The sobel edge detection was coded for the first implementation of the the distortion coeffecient estimation function but is no longer used.
void filter(cv::Mat originalImage , cv::Mat &Filtered, cv::Mat &Direction,  std::string type, std::string Colour){
    cv::Mat Gaussian(3,3, cv::DataType<double>::type), SobelX(3,3, cv::DataType<double>::type) , SobelY(3,3, cv::DataType<double>::type);
    //initial factor
    double factor = 1.0;
    //if a Gaussian blur is desired the input image should be either BGR or Grayscale and must be specified.
    if (type == "Blur") {
        //Gaussian kernel with factor
        Gaussian.at<double>(0,0) = 1;
        Gaussian.at<double>(0,1) = 2;
        Gaussian.at<double>(0,2) = 1;
        Gaussian.at<double>(1,0) = 2;
        Gaussian.at<double>(1,1) = 4;
        Gaussian.at<double>(1,2) = 2;
        Gaussian.at<double>(2,0) = 1;
        Gaussian.at<double>(2,1) = 2;
        Gaussian.at<double>(2,2) = 1;
        factor = 1.0/16.0;
        
        //Blurs a gray image
        if(Colour == "Gray"){
            std::cout << "Applying Gaussian blur to gray image" << std::endl;
            for(int x = 0; x < originalImage.cols; x++){
                for(int y = 0; y < originalImage.rows; y++){
                    double gray_intensity = 0.0;
                    // looping through the image and colvolving with the kernel. The egdes of the images are wrapped around rather than truncated
                    for(int filterY = 0; filterY < 3; filterY++){
                        for(int filterX = 0; filterX < 3; filterX++){
                            int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                            int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                            gray_intensity += (double)originalImage.at<uchar>(imageY, imageX)*Gaussian.at<double>(filterY,filterX);
                        }
                    }
                    // The resulting intensity is capped between 0 and 255 for display purposes.
                    Filtered.at<uchar>(y, x) = (uchar)fmin(fmax(int(factor*gray_intensity), 0), 255);
                }
            }
        }
        // Blurs a colour image
        else if(Colour == "BGR"){
            for(int x = 0; x < originalImage.cols; x++){
                for(int y = 0; y < originalImage.rows; y++){
                    double blueVal = 0.0, greenVal = 0.0, redVal = 0.0;
                    
                    // looping through the image and colvolving with the kernel. The egdes of the images are wrapped around rather than truncated
                    for(int filterY = 0; filterY < 3; filterY++){
                        for(int filterX = 0; filterX < 3; filterX++){
                            int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                            int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                            blueVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[0]*Gaussian.at<double>(filterY,filterX);
                            greenVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[1]*Gaussian.at<double>(filterY,filterX);
                            redVal += (double)originalImage.at<cv::Vec3b>(imageY, imageX)[2]*Gaussian.at<double>(filterY,filterX);
                        }
                    }
                    // The resulting intensities is capped between 0 and 255 for display purposes.
                    Filtered.at<cv::Vec3b>(y, x)[0] = (uchar)fmin(fmax(int(factor*blueVal), 0), 255);
                    Filtered.at<cv::Vec3b>(y, x)[1] = (uchar)fmin(fmax(int(factor*greenVal), 0), 255);
                    Filtered.at<cv::Vec3b>(y, x)[2] = (uchar)fmin(fmax(int(factor*redVal), 0), 255);
                }
            }
        }
    }
    // Sobel edge detector.
    else if(type == "Sobel"){
        // Kernel for derivative with respect to X
        SobelX.at<double>(0,0) = -1;
        SobelX.at<double>(0,1) = 0;
        SobelX.at<double>(0,2) = 1;
        SobelX.at<double>(1,0) = -2;
        SobelX.at<double>(1,1) = 0;
        SobelX.at<double>(1,2) = 2;
        SobelX.at<double>(2,0) = -1;
        SobelX.at<double>(2,1) = 0;
        SobelX.at<double>(2,2) = 1;
        
        // Kernel for derivative with respect to Y
        SobelY.at<double>(0,0) = -1;
        SobelY.at<double>(0,1) = -2;
        SobelY.at<double>(0,2) = -1;
        SobelY.at<double>(1,0) = 0;
        SobelY.at<double>(1,1) = 0;
        SobelY.at<double>(1,2) = 0;
        SobelY.at<double>(2,0) = 1;
        SobelY.at<double>(2,1) = 2;
        SobelY.at<double>(2,2) = 1;
        
        for(int x = 0; x < originalImage.cols; x++){
            for(int y = 0; y < originalImage.rows; y++){
                double Gx = 0.0, Gy = 0.0;
                // looping through the image and colvolving with the kernels. The egdes of the images are wrapped around rather than truncated
                for(int filterY = 0; filterY < 3; filterY++){
                    for(int filterX = 0; filterX < 3; filterX++){
                        int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                        int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                        Gx += (double)originalImage.at<uchar>(imageY, imageX)*SobelX.at<double>(filterY,filterX);
                        Gy += (double)originalImage.at<uchar>(imageY, imageX)*SobelY.at<double>(filterY,filterX);
                    }
                }
                //The Magnitude and direction of the sobel output is truncated between 0 and 255 for display purposes.
                Filtered.at<uchar>(y, x) = (uchar)fmin(fmax(int(sqrt(pow(Gx, 2) + pow(Gy, 2))), 0), 255);;
                Direction.at<uchar>(y, x) = (uchar)int(atan(Gy/Gx));
            }
        }
    }
    
    
    return;
    
}

// Setup calibration by loading each image, finding the chessboard corners and saving the values to the relevant vector of vectors.
void setupCalibration(std::vector<std::vector<cv::Point3f >> &object_points, std::vector<std::vector<cv::Point2f >> &image_points
                      ,int board_width, int board_height, int num_imgs,int &num_valid, double square_size, std::string side, cv::Mat &image, std::string folder, int Debug){
    // Image size. number of chessboard corners (internal corners only)
    cv::Size board_size = cv::Size(board_width, board_height);
    
    // Loop through each image in working directory
    for (int k = 1; k <= num_imgs; k++) {
        std::string img_file;
        
        //Used for debugging. After I rectify the images I first test the rectification with a chessboard image and use this code to detect the corners in order to determine if the corners line up.
        if(Debug == 0){
            img_file = folder + side + std::to_string(k) + ".jpg";
        }
        else{
            img_file = side + ".png";
//            img_file = folder + side + std::to_string(74) + ".jpg";
        }
        // read Image
        image = cv::imread(img_file);
        
        //Check if the image was loaded succsefully.
        if(image.rows == 0){
            printf("Invalid file %s image : %d\n", side.c_str(), k);
            num_valid --;
            continue;
        }
        // Matrices used if filtering is performed.
        cv::Mat BlurIm(image.rows, image.cols, CV_8UC3), gray_temp(image.rows, image.cols, CV_8U), gray_image(image.rows, image.cols , CV_8U), SobelMag(image.rows, image.cols, CV_8U), SobelDirec(image.rows, image.cols, CV_8U);

        // Convert image to grayscale
        cv::cvtColor(image, gray_temp, cv::COLOR_BGR2GRAY);
        
        //Code to filter and apply the sobel edge detector
//        filter(image, BlurIm, SobelDirec, "Blur", "BGR");
//        Filter(gray_temp, gray_image, SobelDirec, "Blur", "Gray");
//        cv::imwrite(Blur_Dir + side + std::to_string(k) + "GrayBlur.png", gray_image);
//        cv::imwrite(Blur_Dir + side + std::to_string(k) + ".png", BlurIm);
//
//        Filter(gray_image, SobelMag, SobelDirec, "Sobel", "Gray");
//        cv::imwrite(Sobel_Dir + side + std::to_string(k) + "Mag.png", SobelMag);
//        cv::imwrite(Sobel_Dir + side + std::to_string(k) + "Dir.png", SobelDirec);
//        
        // Varibale to check if corners have been found
        bool found = false;
        // Finds chessboard corners and returns their 2D coordinates using adaptive thresholding
        found = cv::findChessboardCorners(image, board_size, corners, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FILTER_QUADS);
        
        // If the corners of the chessboard have been found then:
        if(found == true){
            // further refines the corners detected above by searching smaller regions
            cv::cornerSubPix(gray_temp, corners, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 10, 0.001));
//
            // Temp variable for storing the object points
            std::vector< cv::Point3f > obj;
            
            // Iterates through the number of images and number of corners and creates a matrix spaced apart by the chessboard square size. Sets the Z value to 0 without loss of generality to the Maths
            for (int i = 0; i < board_height; i++){
                for (int j = 0; j < board_width; j++){
                    obj.push_back(cv::Point3f((double)j * square_size, (double)i * square_size, 0));
                }
            }
            
            // Store the corners and object points for the current image.
            printf("Found corners in %s image : %d\n", side.c_str(), k);
            image_points.push_back(corners);
            object_points.push_back(obj);
            
            // Draw and display the corners
            cv::drawChessboardCorners(image, board_size, corners, found);
            cv::imwrite(Debug_Dir + side + std::to_string(k) + ".png", image);
        }
        else{
            printf("No corners found in %s image : %d\n", side.c_str(), k);
            num_valid --;
        }
    }
    return;
}

// computes the Matrix used for normalising the object and image points
void getNormalisingMatrix(std::vector<cv::Mat> &Normalised, cv::Mat pts, int num_corners, int check){
    double sum_x = 0.0, sum_y = 0.0, mean_x = 0.0, mean_y = 0.0, var_x = 0.0, var_y = 0.0, s_x = 0.0, s_y = 0.0;
    
    cv::Mat Norm(3, 3, cv::DataType<double>::type, cv::Scalar(0));
    
    // calculate sum of the corner values, which is used to find the average
    for(int i = 0; i < num_corners; i ++){
        sum_x += pts.at<double>(0, i);
        sum_y += pts.at<double>(1, i);
    }
    // Calculating the X and Y average of the image or object points
    mean_x = sum_x/num_corners;
    mean_y = sum_y/num_corners;
    
    // Calculate the x and y variace of the points
    for(int i = 0; i < num_corners; i++){
        var_x += pow(pts.at<double>(0, i) - mean_x, 2);
        var_y += pow(pts.at<double>(1, i) - mean_y, 2);
    }
    var_x = var_x/num_corners;
    var_y = var_y/num_corners;
    
    // calculating values used for normilasation
    s_x = sqrt(2/var_x);
    s_y = sqrt(2/var_y);
    
    //Populating the normalising matrix
    Norm.at<double>(0, 0) = s_x;
    Norm.at<double>(0, 1) = 0.0;
    Norm.at<double>(0, 2) = -s_x*mean_x;
    Norm.at<double>(1, 0) = 0.0;
    Norm.at<double>(1, 1) = s_y;
    Norm.at<double>(1, 2) = -s_y*mean_y;
    Norm.at<double>(2, 0) = 0.0;
    Norm.at<double>(2, 1) = 0.0;
    Norm.at<double>(2, 2) = 1.0;
    
    // saving the matrix to the relevant vector.
    Normalised.push_back(Norm);
    return;
}

// Normalise the image and object points.
void normalisePoints(std::vector<cv::Mat> &Normalised_x, std::vector<cv::Mat> &Normalised_u, std::vector<std::vector<cv::Point2f>> image_points, std::vector<std::vector<cv::Point3f >> object_points, std::vector<std::vector<cv::Point3f>> &Norm_Img, std::vector<std::vector<cv::Point3f>> &Norm_Obj,
               int num_corners, int num_views){
    // Vector for storing the normalised values for a particular image.
    std::vector< cv::Point3f > Norm_Object_Element, Norm_Image_Element;
    
    // Loop through each image and normalise the points
    for(int i = 0; i < num_views; i++){
        
        // Extended image points saved to the temporary matrix ImPoints
        cv::Mat ImPoints(3, num_corners, cv::DataType<double>::type, cv::Scalar(0));
        for (int a = 0; a < num_corners; a++) {
            ImPoints.at<double>(0, a) = image_points[i][a].x;
            ImPoints.at<double>(1, a) = image_points[i][a].y;
            ImPoints.at<double>(2, a) = 1;
        }
        // Extended object points saved to the temporary matrix ObjPoints assuming Z = 0
        cv::Mat ObjPoints(3, num_corners, cv::DataType<double>::type, cv::Scalar(0));
        for (int b = 0; b < num_corners; b++) {
            ObjPoints.at<double>(0, b) = object_points[i][b].x;
            ObjPoints.at<double>(1, b) = object_points[i][b].y;
            ObjPoints.at<double>(2, b) = 1;
        }
        
        // Calculate Normalising Matrices
        getNormalisingMatrix(Normalised_x, ObjPoints, num_corners, 0);
        getNormalisingMatrix(Normalised_u, ImPoints, num_corners, 1);
        
        // Clear Matrix elements
        Norm_Image_Element.clear();
        Norm_Object_Element.clear();
        
        // Normalise each corner in each view
        for(int x = 0; x < num_corners; x ++){
            // Mulitplying by the Normlaising Matrix for the respective view to find the normalised points
            cv::Mat Norm_Object = Normalised_x[i]*ObjPoints.col(x);
            cv::Mat Norm_Image = Normalised_u[i]*ImPoints.col(x);
            
            // Storing the normalised values for current point
            Norm_Image_Element.push_back(cv::Point3f(Norm_Image.at<double>(0), Norm_Image.at<double>(1), Norm_Image.at<double>(2)));
            Norm_Object_Element.push_back(cv::Point3f(Norm_Object.at<double>(0), Norm_Object.at<double>(1), Norm_Object.at<double>(2)));
        }
        // Storing the normalised values for each view
        Norm_Img.push_back(Norm_Image_Element);
        Norm_Obj.push_back(Norm_Object_Element);
    }
    return;
}

//Finds the minimum value of a Matrix. Currently this is used for identifying the index of the lowest singular value found by Single Value Decomposition (SVD)
int findMin(cv::Mat S){
    int index = 0;
    int size = S.rows;
    double temp = S.at<double>(0);
    
    for(int i = 0; i < size; i ++){
        if(S.at<double>(i) < temp){
            temp = S.at<double>(i);
            index = i;
        }
    }
    return index;
}

//Calculate the homography for each image.
void computeHomography(std::vector<cv::Mat> &Homography, std::vector<cv::Mat> Normalised_x, std::vector<cv::Mat> Normalised_u
                       ,std::vector<std::vector<cv::Point3f>>Norm_Img
                       , std::vector<std::vector<cv::Point3f>>Norm_Obj, int view, int num_corners){
    
    int argmin;
    cv::Mat M(2*num_corners, 9, cv::DataType<double>::type);
    cv::Mat U(2*num_corners, 2*num_corners, cv::DataType<double>::type);
    cv::Mat S(1,9, cv::DataType<double>::type);
    cv::Mat Vh(9, 9, cv::DataType<double>::type);
    cv::Mat Norm_h(3, 3, cv::DataType<double>::type);
    cv::Mat H_temp1(3, 3, cv::DataType<double>::type);
    cv::Mat H(3, 3, cv::DataType<double>::type);
    double X = 0.0, Y = 0.0, Z = 0.0, u = 0.0, v = 0.0;
    
    // Itterate for every corner
    for(int corner = 0; corner < num_corners; corner ++){
        // extract the X,Y,Z coridinates for the image and object points
        X = Norm_Obj[view][corner].x;
        Y = Norm_Obj[view][corner].y;
        Z = Norm_Obj[view][corner].z;
        u = Norm_Img[view][corner].x;
        v = Norm_Img[view][corner].y;
        
        // Set up M Matrix for even rows
        M.at<double>(2*corner, 0) = X;
        M.at<double>(2*corner, 1) = Y;
        M.at<double>(2*corner, 2) = 1.0;
        M.at<double>(2*corner, 3) = 0.0;
        M.at<double>(2*corner, 4) = 0.0;
        M.at<double>(2*corner, 5) = 0.0;
        M.at<double>(2*corner, 6) = -X*u;
        M.at<double>(2*corner, 7) = -Y*u;
        M.at<double>(2*corner, 8) = -u;
        
        // Set up M Matrix for odd rows
        M.at<double>(2*corner+1, 0) = 0.0;
        M.at<double>(2*corner+1, 1) = 0.0;
        M.at<double>(2*corner+1, 2) = 0.0;
        M.at<double>(2*corner+1, 3) = X;
        M.at<double>(2*corner+1, 4) = Y;
        M.at<double>(2*corner+1, 5) = 1.0;
        M.at<double>(2*corner+1, 6) = -X*v;
        M.at<double>(2*corner+1, 7) = -Y*v;
        M.at<double>(2*corner+1, 8) = -v;
        
    }
    // Decompose Matrix M into the left (U) and right (Vh) singular matrices along with the singular (S) values
    cv::SVD::compute(M, S, U, Vh, cv::SVD::FULL_UV);

    // find the minimum singular value. By default the are arranged in decending order but this is just a sanity check
    argmin = findMin(S);
    
    // Normalised Homography matrix = to the row in Vh corresponding to the row of the min singular value in S
    H.at<double>(0, 0) = Vh.at<double>(argmin, 0);
    H.at<double>(0, 1) = Vh.at<double>(argmin, 1);
    H.at<double>(0, 2) = Vh.at<double>(argmin, 2);
    H.at<double>(1, 0) = Vh.at<double>(argmin, 3);
    H.at<double>(1, 1) = Vh.at<double>(argmin, 4);
    H.at<double>(1, 2) = Vh.at<double>(argmin, 5);
    H.at<double>(2, 0) = Vh.at<double>(argmin, 6);
    H.at<double>(2, 1) = Vh.at<double>(argmin, 7);
    H.at<double>(2, 2) = Vh.at<double>(argmin, 8);
    
    // Calculate the Non-normalised Homography Matrix
    H_temp1 = (Normalised_u[view].inv())*H*Normalised_x[view];
   
    //Normalising the matrix with respect to the bottom right value in the matrix
    Norm_h = H_temp1/H_temp1.at<double>(2,2);

    // Store homography for current view.
    Homography.push_back(Norm_h);
    
    return;
}

//Used to calculate the B matrix. Refer to Maths for intrinsic calibration
void v_pq(std::vector<cv::Mat> Homography, cv::Mat V_final, int index ){
    
    // Populating the even rows of the V matrix
    V_final.at<double>(2*index, 0) = Homography[index].at<double>(0,0) * Homography[index].at<double>(0,1);
    V_final.at<double>(2*index, 1) = (Homography[index].at<double>(0,0) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(0,1));
    V_final.at<double>(2*index, 2) = Homography[index].at<double>(1,0) * Homography[index].at<double>(1,1);
    V_final.at<double>(2*index, 3) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(0,1)) + (Homography[index].at<double>(0,0) * Homography[index].at<double>(2,1));
    V_final.at<double>(2*index, 4) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(2,1));
    V_final.at<double>(2*index, 5) = Homography[index].at<double>(2,0) * Homography[index].at<double>(2,1);
    
    // Populating the odd rows of the V matrix
    V_final.at<double>(2*index + 1, 0) = (Homography[index].at<double>(0,0) * Homography[index].at<double>(0,0)) - (Homography[index].at<double>(0,1) * Homography[index].at<double>(0,1));
    V_final.at<double>(2*index + 1, 1) = ((Homography[index].at<double>(0,0) * Homography[index].at<double>(1,0)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(0,0)))-
    ((Homography[index].at<double>(0,1) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,1) * Homography[index].at<double>(0,1)));
    V_final.at<double>(2*index + 1, 2) = (Homography[index].at<double>(1,0) * Homography[index].at<double>(1,0)) - (Homography[index].at<double>(1,1) * Homography[index].at<double>(1,1));
    V_final.at<double>(2*index + 1, 3) = ((Homography[index].at<double>(2,0) * Homography[index].at<double>(0,0)) + (Homography[index].at<double>(0,0) * Homography[index].at<double>(2,0)))-
    ((Homography[index].at<double>(2,1) * Homography[index].at<double>(0,1)) + (Homography[index].at<double>(0,1) * Homography[index].at<double>(2,1)));
    V_final.at<double>(2*index + 1, 4) = ((Homography[index].at<double>(2,0) * Homography[index].at<double>(1,0)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(2,0)))-
    ((Homography[index].at<double>(2,1) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,1) * Homography[index].at<double>(2,1)));
    V_final.at<double>(2*index + 1, 5) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(2,0)) - (Homography[index].at<double>(2,1) * Homography[index].at<double>(2,1));
    return;
}

//Calculate the Intrinsic Parameters of each camera.
cv::Mat intrinsicParameters(std::vector<cv::Mat> Homography, int M){
    
    double w = 0.0, vc = 0.0, alpha = 0.0, beta = 0.0, gamma = 0.0, uc = 0.0, d = 0.0;
    cv::Mat V(2*M, 6, cv::DataType<double>::type);
    cv::Mat H(3, 3, cv::DataType<double>::type);
    cv::Mat U(2*M, 2*M, cv::DataType<double>::type);
    cv::Mat S(1, 6, cv::DataType<double>::type);
    cv::Mat Vh(6, 6, cv::DataType<double>::type);
    cv::Mat B(1, 6, cv::DataType<double>::type);
    cv::Mat K(3, 3, cv::DataType<double>::type);
    
    // Find V matrix which takes all homographies into account for intrinsic calibration
    for(int i = 0; i < M; i++){
        v_pq(Homography, V, i);
    }
    
    // Decompose Matrix V into the left (U) and right (Vh) singular matrices along with the singular (S) values
    cv::SVD::compute(V, S, U, Vh);
    
    // Find the minimum singular value.
    int argmin = findMin(S);
    
    // B matrix = to the row in Vh corresponding to the row of the min singular value in S
    B.at<double>(0) = Vh.at<double>(argmin, 0);
    B.at<double>(1) = Vh.at<double>(argmin, 1);
    B.at<double>(2) = Vh.at<double>(argmin, 2);
    B.at<double>(3) = Vh.at<double>(argmin, 3);
    B.at<double>(4) = Vh.at<double>(argmin, 4);
    B.at<double>(5) = Vh.at<double>(argmin, 5);
    
    // Common factor w used for intrinsic paramter calculation
    w = (B.at<double>(0)*B.at<double>(2)*B.at<double>(5)) - (pow(B.at<double>(1), 2)*B.at<double>(5)) - (B.at<double>(0)*pow(B.at<double>(4), 2)) + (2*B.at<double>(1)*B.at<double>(3)*B.at<double>(4)) - (B.at<double>(2)*pow(B.at<double>(3), 2));
    
    // Common factor w used for intrinsic paramter calculation
    d = (B.at<double>(0)*B.at<double>(2)) - pow(B.at<double>(1), 2);
    
    // focal length in the x direction
    alpha = sqrt(w/(d*B.at<double>(0)));
    
    // focal length in the y direction
    beta = sqrt(w/(pow(d, 2))*B.at<double>(0));
    
    // Camera skew
    gamma = beta*B.at<double>(1);
    
    // horizontal principle point
    uc = ((B.at<double>(1)*B.at<double>(4)) - (B.at<double>(2)*B.at<double>(3)))/d;
    
    // vertical principle point
    vc = ((B.at<double>(1)*B.at<double>(3)) - (B.at<double>(0)*B.at<double>(4)))/d;
    
    // Populate the Intrinsic Matrix
    K.at<double>(0, 0) = alpha;
    K.at<double>(0, 1) = gamma;
    K.at<double>(0, 2) = uc;
    K.at<double>(1, 0) = 0.0;
    K.at<double>(1, 1) = beta;
    K.at<double>(1, 2) = vc;
    K.at<double>(2, 0) = 0.0;
    K.at<double>(2, 1) = 0.0;
     K.at<double>(2, 2) = 1.0;
    
    return K;
}

//Calculate the Extrinsic Parameters of each camera.
void extrinsicParameters(std::vector<cv::Mat> Homography, std::vector<cv::Mat>& Rotation, std::vector<cv::Mat>& Extrinsic,  std::vector<cv::Mat>& Translation, cv::Mat K, int num_imgs, cv::Mat &C){
    cv::Mat R1(1, 5, cv::DataType<double>::type, cv::Scalar(0)), R2(1, 5, cv::DataType<double>::type, cv::Scalar(0)), R3(1, 5, cv::DataType<double>::type, cv::Scalar(0)), T(1, 5, cv::DataType<double>::type, cv::Scalar(0)), S, U, Vh, Ex(3,4, cv::DataType<double>::type), R(3,3, cv::DataType<double>::type);
    
    //Calculate the Extrinsic Parameters of each camera.
    for(int view = 0; view < 1; view++){
        //Calculate the Scaling factors
        double mu1 = 1/norm(K.inv()*Homography[view].col(0), cv::NORM_L2);
        double mu2 = 1/norm(K.inv()*Homography[view].col(1), cv::NORM_L2);
        double mu3 = (mu1 + mu2)/2;
        
        //Calculate the columns of the rotation matix as given by Zhang, R1 = col 1
        R1 = mu1*K.inv()*Homography[view].col(0);
        R2 = mu2*K.inv()*Homography[view].col(1);
        R3 = R1.cross(R2);
        
        //Calculate the translation matix as given by Zhang
        T = mu3*K.inv()*Homography[view].col(2);
        
        cv::Mat rvec(3, 3, cv::DataType<double>::type);
        rvec.at<double>(0,0) = R1.at<double>(0);
        rvec.at<double>(0,1) = R2.at<double>(0);
        rvec.at<double>(0,2) = R3.at<double>(0);
        rvec.at<double>(1,0) = R1.at<double>(1);
        rvec.at<double>(1,1) = R2.at<double>(1);
        rvec.at<double>(1,2) = R3.at<double>(1);
        rvec.at<double>(2,0) = R1.at<double>(2);
        rvec.at<double>(2,1) = R2.at<double>(2);
        rvec.at<double>(2,2) = R3.at<double>(2);
        
        //Caluclate the SVD of the calcuated rotation matrix above in order to generate a true rotation matrix which satisfies the required properties of a rotation matrix
        cv::SVD::compute(rvec, S, U, Vh);
        
        R = U*Vh;
        std::cout <<"Rot: " <<R << std::endl;
        
        //Extrinsic parameter matrix
        Ex.at<double>(0,0) = R.at<double>(0,0);
        Ex.at<double>(0,1) = R.at<double>(0,1);
        Ex.at<double>(0,2) = R.at<double>(0,2);
        Ex.at<double>(0,3) = T.at<double>(0);
        Ex.at<double>(1,0) = R.at<double>(1,0);
        Ex.at<double>(1,1) = R.at<double>(1,1);
        Ex.at<double>(1,2) = R.at<double>(1,2);
        Ex.at<double>(1,3) = T.at<double>(1);
        Ex.at<double>(2,0) = R.at<double>(2,0);
        Ex.at<double>(2,1) = R.at<double>(2,1);
        Ex.at<double>(2,2) = R.at<double>(2,2);
        Ex.at<double>(2,3) = T.at<double>(2);
        std::cout << "Ex: " << Ex << std::endl;
        //Calculate Camera center in the arbitrary world plane.
        C = -R.inv()*T;
        std::cout << "CamCenter: " << -R.inv()*T << std::endl;
        
        //Store all values calculated above
        Rotation.push_back(R);
        Translation.push_back(T);
        Extrinsic.push_back(Ex);
        
    }
    return;
}

//Saves the paramteres to a folder for further processing by the range extraction program.
void saveFiles(std::string fileName,cv::Mat K1, cv::Mat K2, cv::Mat R1, cv::Mat R2, cv::Mat T1, cv::Mat T2, cv::Mat R, cv::Mat T, cv::Mat C1, cv::Mat C2, cv::Mat Po1, cv::Mat Po2, int board_width, int board_height, double square_size, int num_imgs, int cols, int rows){

    cv::FileStorage File(fileName, cv::FileStorage::WRITE);
    File << "K1" << K1;
    File << "K2" << K2;
    File << "R1" << R1;
    File << "R2" << R2;
    File << "T1" << T1;
    File << "T2" << T2;
    File << "R" << R;
    File << "T" << T;
    File << "C1" << C1;
    File << "C2" << C2;
    File << "Po1" << Po1;
    File << "Po2" << Po2;
    File << "board_width" << board_width;
    File << "board_height" << board_height;
    File << "square_size" << square_size;
    File << "num_imgs" << num_imgs;
    File << "cols" << cols;
    File << "rows" << rows;
    return;
}

//Reprojection errors are computed. Nothing below this point is currently used.
double computeReprojectionErrors(const std::vector< std::vector< cv::Point3f > >& objectPoints,
                                 const std::vector< std::vector< cv::Point2f > >& imagePoints,
                                 const std::vector< cv::Mat >& rvecs, const std::vector< cv::Mat >& tvecs,
                                 const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs, int use_size) {
    std::vector< cv::Point2f > imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    std::vector< float > perViewErrors;
    perViewErrors.resize(objectPoints.size());
    
    for (i = 0; i < use_size; ++i) {
        cv::projectPoints(cv::Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                      distCoeffs, imagePoints2);
        err = norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), cv::NORM_L2);
        int n = use_size;
        perViewErrors[i] = (float) sqrt(err*err/n);
        totalErr += err*err;
        totalPoints += n;
    }
    return sqrt(totalErr/totalPoints);
}

//Code used to estimate the distortion coeffecients.
void estDist(cv::Mat K, std::vector<cv::Mat> Extrinsic, std::vector<std::vector<cv::Point2f>> image_points, std::vector<std::vector<cv::Point3f>> object_points, cv::Mat &k, int num_views, int num_corners){
    cv::Mat D(2*num_views*num_corners, 2, cv::DataType<double>::type), d(2*num_views*num_corners, 1, cv::DataType<double>::type), Temp(3, 1, cv::DataType<double>::type), HomogObjPoint(4, 1, cv::DataType<double>::type), U, S, Vh, diagS(2,2, cv::DataType<double>::type);
    double uc = K.at<double>(0,2);
    double vc = K.at<double>(1,2);
    int l = 0;

    for(int view = 0; view < num_views; view ++){
        for(int corn = 0; corn < num_corners; corn++){
            HomogObjPoint.at<double>(0) = object_points[view][corn].x;
            HomogObjPoint.at<double>(1) = object_points[view][corn].y;
            HomogObjPoint.at<double>(2) = 1;
            HomogObjPoint.at<double>(3) = 1;
            Temp = Extrinsic[view]*HomogObjPoint;
            double x_proj = Temp.at<double>(0);
            double y_proj = Temp.at<double>(1);
            double r = sqrt(pow(x_proj,2) + pow(y_proj,2));
            Temp = K*Extrinsic[view]*HomogObjPoint;
            double u_proj = Temp.at<double>(0);
            double v_proj = Temp.at<double>(1);

            double du = u_proj - uc;
            double dv = v_proj - vc;

            D.at<double>(2*l, 0) = du*pow(r,2);
            D.at<double>(2*l, 1) = du*pow(r,4);
            D.at<double>(2*l + 1, 0) = dv*pow(r,2);
            D.at<double>(2*l + 1, 1) = dv*pow(r,4);

            double u_obs = image_points[view][corn].x;
            double v_obs = image_points[view][corn].y;

            d.at<double>(2*l) = u_obs - u_proj;
            d.at<double>(2*l + 1) = v_obs - v_proj;
            l++;
        }
    }

    cv::SVD::compute(D, S, U, Vh);

    diagS.at<double>(0,0) = 1.0/S.at<double>(0);
    diagS.at<double>(0,1) = 0.0;
    diagS.at<double>(1,0) = 0.0;
    diagS.at<double>(1,1) = 1.0/S.at<double>(1);
    k = Vh*(diagS)*(U.t()*d);
    std::cout << "k: " << k << std::endl;
    return;
    
}

//code used for Interpolation after the inverse mapping from the distortion correction function below.
double BilinearInterpolation(double x1, double x2, double y1, double y2, double x, double y,  double q11,double q12,double q21,double q22 ){
    double x2x1, y1y2, x2x, y1y, yy2, xx1;
    x2x1 = x2 - x1;
    y1y2 = y1 - y2;
    x2x = x2 - x;
    y1y = y1 - y;
    yy2 = y - y2;
    xx1 = x - x1;
    double I1 = ((x2x)/(x2x1))*q12 + ((xx1)/(x2x1))*q22;
    //    cout << "I1: " << I1 << endl;
    double I2 = ((x2x)/(x2x1))*q11 + ((xx1)/(x2x1))*q21;
    //    cout << "I2: " << I2 << endl;
    double I3 = ((y1y)/(y1y2))*I1 + ((yy2)/(y1y2))*I2;
    //    cout << "I3: " << I3 << endl;
    return I3;
}

//Inverse distortion ufnction which is not working
void inverseDist(cv::Mat K, cv::Mat D, int num_imgs, std::string side){
    double rd = 0.0, ru = 0.0, q = 0.0, p = 0.0, delta = 0.0, o_x, o_y, x_d, y_d;
    cv::Mat image, gray_image, undistorted(960, 1280, cv::DataType<double>::type);

    o_x = K.at<double>(0,2);
    o_y = K.at<double>(1,2);

    for(int view = 1; view <= 7; view++) {
        std::string img_file = "../Pictures/" + side + std::to_string(view) + ".jpg";
        image = cv::imread(img_file);
        cv::cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);

        for(int y_u = 0; y_u < 960; y_u ++){
            for(int x_u = 0; x_u < 1280; x_u ++){
                ru = sqrt(pow((x_u - o_x), 2) + pow((y_u - o_y), 2));
                q = -ru/D.at<double>(0);
                p = 1.0/D.at<double>(0);
                delta = pow(q, 2) + (4.0/27.0) * pow(p, 3);
                if(delta < 0){
                    std::cout << "Not possible" << std::endl;
                    continue;
                }
                rd = cbrt((-q + sqrt(delta))/2) + cbrt((-q - sqrt(delta))/2);
                x_d = ((rd/ru)*(x_u - o_x)) + o_x;
                y_d = ((rd/ru)*(y_u - o_y)) + o_y;

                double col11 = floor(x_d);
                double col12 = ceil(x_d);
                double row11 = ceil(y_d);
                double row12 = floor(y_d);

                undistorted.at<double>(y_u, x_u) = BilinearInterpolation(col11, col12, row11, row12, x_d, y_d, gray_image.at<uchar>(row11,col11), gray_image.at<uchar>(row12,col11), gray_image.at<uchar>(row11,col12), gray_image.at<uchar>(row12,col12));

            }
        }
        std::string file = "../Undistorted/" + side + std::to_string(view) + ".jpg";
        cv::imwrite(file, undistorted);

    }

    return;
}




//previous code used for camera centre calculation
//void cameraCentre(cv::Mat C1, cv::Mat C2, cv::Mat K1, cv::Mat K2, cv::Mat R1, cv::Mat R2, cv::Mat T1
//                  , cv::Mat T2, cv::Mat &RelR, cv::Mat &RelT, cv::Mat &Po1, cv::Mat &Po2){
//
//    cv::Mat Ex1(3,4, cv::DataType<double>::type), Ex2(3,4, cv::DataType<double>::type);
//    Ex1.at<double>(0,0) = R1.at<double>(0,0);
//    Ex1.at<double>(0,1) = R1.at<double>(0,1);
//    Ex1.at<double>(0,2) = R1.at<double>(0,2);
//    Ex1.at<double>(0,3) = T1.at<double>(0);
//    Ex1.at<double>(1,0) = R1.at<double>(1,0);
//    Ex1.at<double>(1,1) = R1.at<double>(1,1);
//    Ex1.at<double>(1,2) = R1.at<double>(1,2);
//    Ex1.at<double>(1,3) = T1.at<double>(1);
//    Ex1.at<double>(2,0) = R1.at<double>(2,0);
//    Ex1.at<double>(2,1) = R1.at<double>(2,1);
//    Ex1.at<double>(2,2) = R1.at<double>(2,2);
//    Ex1.at<double>(2,3) = T1.at<double>(2);
//
//    Ex2.at<double>(0,0) = R2.at<double>(0,0);
//    Ex2.at<double>(0,1) = R2.at<double>(0,1);
//    Ex2.at<double>(0,2) = R2.at<double>(0,2);
//    Ex2.at<double>(0,3) = T2.at<double>(0);
//    Ex2.at<double>(1,0) = R2.at<double>(1,0);
//    Ex2.at<double>(1,1) = R2.at<double>(1,1);
//    Ex2.at<double>(1,2) = R2.at<double>(1,2);
//    Ex2.at<double>(1,3) = T2.at<double>(1);
//    Ex2.at<double>(2,0) = R2.at<double>(2,0);
//    Ex2.at<double>(2,1) = R2.at<double>(2,1);
//    Ex2.at<double>(2,2) = R2.at<double>(2,2);
//    Ex2.at<double>(2,3) = T2.at<double>(2);
//
//
//    cv::Mat Qo1(3,3, cv::DataType<double>::type), Qo2(3,3, cv::DataType<double>::type);
//    Po1 = K1*Ex1;
//    Po2 = K2*Ex2;
//    std::cout << "Po1: " << Po1 << std::endl;
//    std::cout << "Po2: " << Po2 << std::endl;
//
//    Qo1.at<double>(0,0) = Po1.at<double>(0,0);
//    Qo1.at<double>(0,1) = Po1.at<double>(0,1);
//    Qo1.at<double>(0,2) = Po1.at<double>(0,2);
//    Qo1.at<double>(1,0) = Po1.at<double>(1,0);
//    Qo1.at<double>(1,1) = Po1.at<double>(1,1);
//    Qo1.at<double>(1,2) = Po1.at<double>(1,2);
//    Qo1.at<double>(2,0) = Po1.at<double>(2,0);
//    Qo1.at<double>(2,1) = Po1.at<double>(2,1);
//    Qo1.at<double>(2,2) = Po1.at<double>(2,2);
//
//    Qo2.at<double>(0,0) = Po2.at<double>(0,0);
//    Qo2.at<double>(0,1) = Po2.at<double>(0,1);
//    Qo2.at<double>(0,2) = Po2.at<double>(0,2);
//    Qo2.at<double>(1,0) = Po2.at<double>(1,0);
//    Qo2.at<double>(1,1) = Po2.at<double>(1,1);
//    Qo2.at<double>(1,2) = Po2.at<double>(1,2);
//    Qo2.at<double>(2,0) = Po2.at<double>(2,0);
//    Qo2.at<double>(2,1) = Po2.at<double>(2,1);
//    Qo2.at<double>(2,2) = Po2.at<double>(2,2);
//
//    C1 = -(Qo1.inv())*Po1.col(3);
//    C2 = -(Qo2.inv())*Po2.col(3);
//
//    //    RelR = R1.t()*R2;
//    //    RelT = R1.t()*(T2 - T1);
//    RelR = R2*R1;
//    RelT = T2 - (RelR*T1);
//    return;
//}





//Code used to find Projection matrix from point correspondances but this did not work.
//void ProjectionMat(std::vector<std::vector<cv::Point2f>>Norm_Img, std::vector<std::vector<cv::Point3f>>Norm_Obj, cv::Mat P1, cv::Mat K1, cv::Mat T1, cv::Mat R1, int num_corners, int index){
//int argmin;
//cv::Mat A(2*num_corners, 12, cv::DataType<double>::type);
//cv::Mat U(2*num_corners, 2*num_corners, cv::DataType<double>::type);
//cv::Mat S(1,12, cv::DataType<double>::type);
//cv::Mat Vh(12, 12, cv::DataType<double>::type);
//cv::Mat P1norm(3, 4, cv::DataType<double>::type);
//double X = 0.0, Y = 0.0, Z = 0.0, u = 0.0, v = 0.0;
//
//// Itterate for every corner
//for(int i = 0; i < num_corners; i ++){
//    // extract the X,Y,Z coridinates for the image and object points
//    X = Norm_Obj[index][i].x;
//    Y = Norm_Obj[index][i].y;
//    Z = Norm_Obj[index][i].z;
//    u = Norm_Img[index][i].x;
//    v = Norm_Img[index][i].y;
//
//    // Set up M Matrix for even rows
//    A.at<double>(2*i, 0) = X;
//    A.at<double>(2*i, 1) = Y;
//    A.at<double>(2*i, 2) = Z;
//    A.at<double>(2*i, 3) = 1;
//    A.at<double>(2*i, 4) = 0.0;
//    A.at<double>(2*i, 5) = 0.0;
//    A.at<double>(2*i, 6) = 0.0;
//    A.at<double>(2*i, 7) = 0.0;
//    A.at<double>(2*i, 8) = -X*u;
//    A.at<double>(2*i, 9) = -Y*u;
//    A.at<double>(2*i, 10) = -Z*u;
//    A.at<double>(2*i, 11) = -u;
//
//    // Set up M Matrix for odd rows
//    A.at<double>(2*i+1, 0) = 0.0;
//    A.at<double>(2*i+1, 1) = 0.0;
//    A.at<double>(2*i+1, 2) = 0.0;
//    A.at<double>(2*i+1, 3) = 0.0;
//    A.at<double>(2*i+1, 4) = X;
//    A.at<double>(2*i+1, 5) = Y;
//    A.at<double>(2*i+1, 6) = Z;
//    A.at<double>(2*i+1, 7) = 1.0;
//    A.at<double>(2*i+1, 8) = -X*v;
//    A.at<double>(2*i+1, 9) = -Y*v;
//    A.at<double>(2*i+1, 10) = -Z*v;
//    A.at<double>(2*i+1, 11) = -v;
//
//}
//// Decompose Matrix M into the left (U) and right (Vh) singular matrices along with the singular (S) values
//cv::SVD::compute(A, S, U, Vh, cv::SVD::FULL_UV);
//std::cout << "New S: " << S << std::endl;
//std::cout << "New U: " << U << std::endl;
//std::cout << "New Vh: " << Vh << std::endl;
////    std::cout << "Vh size: " <<Vh.size << std::endl;
//// Find the minimum singular value.
//argmin = findMin(S);
//
//// Non-Normalised homography matrix = to the row in Vh corresponding to the row of the min singular value in S
//P1norm.at<double>(0, 0) = Vh.at<double>(argmin, 0);
//P1norm.at<double>(0, 1) = Vh.at<double>(argmin, 1);
//P1norm.at<double>(0, 2) = Vh.at<double>(argmin, 2);
//P1norm.at<double>(0, 3) = Vh.at<double>(argmin, 3);
//P1norm.at<double>(1, 0) = Vh.at<double>(argmin, 4);
//P1norm.at<double>(1, 1) = Vh.at<double>(argmin, 5);
//P1norm.at<double>(1, 2) = Vh.at<double>(argmin, 6);
//P1norm.at<double>(1, 3) = Vh.at<double>(argmin, 7);
//P1norm.at<double>(2, 0) = Vh.at<double>(argmin, 8);
//P1norm.at<double>(2, 1) = Vh.at<double>(argmin, 9);
//P1norm.at<double>(2, 2) = Vh.at<double>(argmin, 10);
//P1norm.at<double>(2, 3) = Vh.at<double>(argmin, 11);
//
//std::cout << "New P1: " << P1norm << std::endl;
//
//cv::decomposeProjectionMatrix(P1norm, K1, R1, T1);
//
//std::cout << "New K1: " << K1 << std::endl;
//std::cout << "New R1: " << R1 << std::endl;
//std::cout << "New T1: " << T1 << std::endl;
//
//return;
//}
//








    
// Code used to attempt the paramter refinement through the levenberg Marq Algorithm implemented with the cminpack library.
    //cv::Mat Val(cv::Mat H, std::vector<cv::Point3f> object ,int num_corners){
    //    cv::Mat Y(2*num_corners, 1, cv::DataType<double>::type);
    //    for(int i = 0; i < num_corners; i ++){
    //        double w = (H.at<double>(2,0)*object[i].x) + (H.at<double>(2,1)*object[i].y) + H.at<double>(2,2);
    //        double u = (1.0/w)*((H.at<double>(0,0)*object[i].x) + (H.at<double>(0,1)*object[i].y) + (H.at<double>(0,2)));
    //        double v = (1.0/w)*((H.at<double>(1,0)*object[i].x) + (H.at<double>(1,1)*object[i].y) + (H.at<double>(1,2)));
    //        Y.at<double>(2*i) = u;
    //        Y.at<double>(2*i + 1) = v;
    //    }
    //    std::cout << "Y: " << Y << std::endl;
    //    return Y;
    //}

    //void FCN(M,N,X,FVEC,FJAC,LDFJAC,IFLAG){
    //
    //    return;
    //}

    //cv::Mat Jac(cv::Mat H, std::vector<cv::Point3f> object ,int num_corners){
    //    cv::Mat J(2*num_corners, 9, cv::DataType<double>::type);
    //    for(int i = 0; i < num_corners; i ++){
    //        double sx = (H.at<double>(0,0)*object[i].x) + (H.at<double>(0,1)*object[i].y) + H.at<double>(0,2);
    //        double sy = (H.at<double>(1,0)*object[i].x) + (H.at<double>(1,1)*object[i].y) + H.at<double>(1,2);
    //        double w = (H.at<double>(2,0)*object[i].x) + (H.at<double>(2,1)*object[i].y) + H.at<double>(2,2);
    //        J.at<double>(2*i, 0) = object[i].x/w;
    //        J.at<double>(2*i, 1) = object[i].y/w;
    //        J.at<double>(2*i, 2) = 1.0/w;
    //        J.at<double>(2*i, 3) = 0;
    //        J.at<double>(2*i, 4) = 0;
    //        J.at<double>(2*i, 5) = 0;
    //        J.at<double>(2*i, 6) = -(sx*object[i].x)/(pow(w, 2));
    //        J.at<double>(2*i, 7) = -(sx*object[i].y)/(pow(w, 2));
    //        J.at<double>(2*i, 8) = -(sx)/(pow(w, 2));
    //
    //        J.at<double>(2*i + 1, 0) = 0;
    //        J.at<double>(2*i + 1, 1) = 0;
    //        J.at<double>(2*i + 1, 2) = 0;
    //        J.at<double>(2*i + 1, 3) = object[i].x/w;
    //        J.at<double>(2*i + 1, 4) = object[i].y/w;
    //        J.at<double>(2*i + 1, 5) = 1.0/w;
    //        J.at<double>(2*i + 1, 6) = -(sy*object[i].x)/(pow(w, 2));
    //        J.at<double>(2*i + 1, 7) = -(sy*object[i].y)/(pow(w, 2));
    //        J.at<double>(2*i + 1, 8) = -(sy)/(pow(w, 2));
    //    }
    //    std::cout << "J: " << J << std::endl;
    //    return J;
    //}

    //void refineHomog(std::vector<cv::Mat> &Homography,std::vector< std::vector< cv::Point3f > > object_points, int num_corners, int num_views){
    //    static int num_var = 9;
    //    int J, LDFJAC = num_var, MAXFEV = 400, MODE = 1, NPRINT = 0, INFO, *NFEV, *NJEV, NWRITE;
    //    int IPVT[num_var];
    //    double FTOL = sqrt(dpmpar(1)), XTOL = sqrt(dpmpar(1)), GTOL = 0, FACTOR = 100.0, FNORM, ENORM, DPMPAR;
    //    double X[num_var], FVEC[num_corners], FJAC[num_corners][num_var], DIAG[num_var], QTF[num_var], WA1[num_var], WA2[num_var], WA3[num_var], WA4[num_corners];
    //    void* p=NULL;
    //    for(int view = 0; view < num_views; view++){
    //        for(int t = 0; t < num_var; t++){
    //            X[t] = Homography[view].at<double>(t);
    //        }
    //    }
    //
    //
    ////    lmder( FCN(), num_corners, num_var, X, FVEC, FJAC, 2*num_corners, FTOL, XTOL, GTOL, MAXFEV, DIAG, MODE, FACTOR, NPRINT, NFEV, NJEV, IPVT, QTF, WA1, WA2, WA3, WA4);
    //
    //    Val(Homography[0], object_points[0], num_corners);
    //    Jac(Homography[0], object_points[0], num_corners);
    //
    //
    //    return;
    //}
//}

#endif /* IntrinsicCalibration_h */



