//
//  main.cpp
//  Calibration
//
//  Created by Mathew Strydom on 2019/08/20.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include "IntrinsicCalibration.h"
#include <vector>
#include <cstring>
#include <cminpack.h>

//Internal corners of the chessboard
#define board_width 9
#define board_height 6
//Number of images captured per camera
#define num_imgs 70
//chessboard single square size in cm
#define square_size 1 // 1 changed for Samples
#define debug 0

// Vector of vectors for object points (World points of the chessboard), normalised image points and normalised object points which stores 3D coordinates
std::vector< std::vector< cv::Point3f > > left_object_points, left_norm_img_pts, left_norm_obj_pts
,right_object_points, right_norm_img_pts, right_norm_obj_pts;
// Vector of Vectors for image_points which stores 2D coordinates (2D cooridantes of the chessboard corners for each image)
std::vector< std::vector< cv::Point2f > > left_image_points, right_image_points;
// Normalising matrix for the object (x) and image points(u) and its inverse. Homography matrix for each image, Extrinsic paramter Matrices
std::vector < cv::Mat > left_normalised_obj_pts, left_normalised_img_pts, left_homography, left_rotation, left_translation, left_extrinsic
,right_normalised_obj_pts, right_normalised_img_pts,  right_homography, right_rotation, right_translation, right_extrinsic;

cv::Mat left_img, right_img, left_undis, right_undis;

int main(int argc, const char * argv[]) {
    
    //Internal corners of the chessboard
    int num_corners = board_width*board_height;
    
    std::string left = "left";
    std::string right = "right";
    std::string image_folder = "../Pictures_V3/";
    std::string stereo_YML_file = "../Camera_files/cam_stereo.yml";
    std::string stereo_TXT_file = "../Camera_files/cam_stereo.txt";
    
    // K1 and K2 are the Intrinsic Camera Matrices of the left and right camera respectively
    cv::Mat K1(3,3, cv::DataType<double>::type), K2(3,3, cv::DataType<double>::type);
    
    // C1 and C2 are the camera centers in the arbituary world plane
    cv::Mat C1(3,1, cv::DataType<double>::type), C2(3,1, cv::DataType<double>::type);
    
     // Po1 and Po2 are the projection Matrices of the left and right camera respectively
    cv::Mat Po1(4,4, cv::DataType<double>::type), Po2(4,4, cv::DataType<double>::type);
    
    // R1 and R2 are the rotation Matrices of the left and right camera respectively, T1 and T2 are the translation matrices of the left and right camera respectively. R and T are the rotation and translation matrices of the right camera with respect to the left camera.
    cv::Mat R1, R2, T1, T2 ,R, T;
    
    // Number of valid images taken per camera. (Some images are corrupted. I think this is an issue with the the clock syncronisation but it is not my main concern at the moment)
    int num_valid_left_imgs = num_imgs;
    int num_valid_right_imgs = num_imgs;
    // lower number of images which were succesfully imported from the above values.
    int use_images = 0;
    printf("Finding Corners \n");
    // Finds the chessboard corners for each image from each camera. This is done with OpenCV at the moment but it will have to be done from first principles before presenting at the final demo.
    setupCalibration(left_object_points, left_image_points, board_width, board_height, num_imgs, num_valid_left_imgs, square_size, left, left_img, image_folder, debug);
    setupCalibration(right_object_points, right_image_points, board_width, board_height, num_imgs, num_valid_right_imgs, square_size, right, right_img, image_folder, debug);
    
    if (num_valid_left_imgs < num_valid_right_imgs) {
        use_images = num_valid_left_imgs;
    }
    else{
        use_images = num_valid_right_imgs;
    }
    printf("Starting Calibration\n");
   
    // Normalise image and object points for each image
    normalisePoints(left_normalised_obj_pts, left_normalised_img_pts, left_image_points, left_object_points, left_norm_img_pts, left_norm_obj_pts
              , num_corners, use_images);
    normalisePoints(right_normalised_obj_pts, right_normalised_img_pts, right_image_points, right_object_points, right_norm_img_pts, right_norm_obj_pts
              , num_corners, use_images);
    
    printf("Normilisation Complete.\n");
    printf("Number of Views Normalised:\t %d\n", use_images);
    printf("Number of Points per view:\t %d\n", num_corners);
    
    printf("Computing homography per view.\n");
    
    // Compute the homography matrix for each image
    for(int view = 0; view < use_images; view++){
        computeHomography(left_homography, left_normalised_obj_pts, left_normalised_img_pts, left_norm_img_pts, left_norm_obj_pts
                          , view, num_corners);
        computeHomography(right_homography, right_normalised_obj_pts, right_normalised_img_pts, right_norm_img_pts, right_norm_obj_pts
                          , view, num_corners);
    }
    //attempt at the non linear optimisation of the homographies
//    refineHomog(left_homography, left_object_points, num_corners, use_images);
    
    printf("Computing Intrinsic Camera Parameters.\n");
    // Calculation of the Intrinsic camera parameters
    K1 = intrinsicParameters(left_homography, use_images);
    K2 = intrinsicParameters(right_homography, use_images);
    std::cout << "K1:\n" << K1 << std::endl;
    std::cout << "K2:\n" << K2 << std::endl;
    
    //using the first images homography for the extrinsic paramter calcualtion.
    int view = 0;
    printf("\nComputing Extrinsic Camera Parameters for the left Camera.\n");
    extrinsicParameters(left_homography, left_rotation, left_extrinsic, left_translation, K1, use_images, C1);
    printf("\nComputing Extrinsic Camera Parameters for the right Camera.\n");
    extrinsicParameters(right_homography, right_rotation, right_extrinsic, right_translation, K2, use_images, C2);
    std::cout << "R1:\t" << left_rotation[view] << std::endl;
    std::cout << "R2:\t" << right_rotation[view] << std::endl;
    
    R1 = left_rotation[view];
    T1 = left_translation[view];
    R2 = right_rotation[view];
    T2 = right_translation[view];
    std::cout << "Ex1: " << left_extrinsic[view] << std::endl;
    
    //Calculation of the original projection matrices
    Po1 = K1*left_extrinsic[view];
    Po2= K2*right_extrinsic[view];
    std::cout << "Po1: " << Po1 << std::endl;
    std::cout << "Po2: " << Po2 << std::endl;
    
    //Calculation of the relative camera position
    R = R1.inv()*R2;
    T = R1.inv()*(T2-T1);
    std::cout << "R: " << R << std::endl;
    std::cout << "T: " << T << std::endl;
    
    //An Attempt at removing the distortion from the images
//    cv::Mat D1(2,1, cv::DataType<double>::type), D2(2,1, cv::DataType<double>::type);
    // Estimate the distortion paramters using projection errors
//    estDist(K1, left_extrinsic, left_image_points, left_object_points, D1, use_images, num_corners);
    //inverse mapping with the distortion paramter to generate the undistorted image
//    inverseDist(K1, D1, use_images, left);

    //Presenting the calibration results.
    std::cout << "\nTotal Number of Horizontal pixels from OV2640 datasheet: 1632" << std::endl;
    std::cout << "Total Number of Vertical pixelsfrom OV2640 datasheet: 1220" << std::endl;
    std::cout << "Camera 1: Principle point -> \t(" << K1.at<double>(0,2) << ", " <<  K1.at<double>(1,2) << ")\t(units in pixels)" << std::endl;
    std::cout << "Camera 1: Theoretical Centre -> (" << left_img.cols/2 << ", " <<  left_img.rows/2 << ")\t\t\t(units in pixels)" << std::endl;
    std::cout << "Camera 1: focal length -> \t\t(" << K1.at<double>(0,0) << ", " <<  K1.at<double>(1,1) << ")\t(units in pixels)" << std::endl;
    std::cout << "Camera 1: focal length -> \t\t(" << K1.at<double>(0,0)*0.0022 << ", " <<  K1.at<double>(1,1)*0.0022 << ")\t(units in mm)" << std::endl;
    std::cout << "Camera 2: Principle point -> \t(" << K2.at<double>(0,2) << ", " <<  K2.at<double>(1,2) << ")\t(units in pixels)" << std::endl;
    std::cout << "Camera 2: Theoretical Centre -> (" << left_img.cols/2 << ", " <<  left_img.rows/2 << ")\t\t\t(units in pixels)" << std::endl;
    std::cout << "Camera 2: focal length -> \t\t(" << K2.at<double>(0,0) << ", " <<  K2.at<double>(1,1) << ")\t(units in pixels)" << std::endl;
    std::cout << "Camera 2: focal length -> \t\t(" << K2.at<double>(0,0)*0.0022 << ", " <<  K2.at<double>(1,1)*0.0022 << ")\t(units in mm)" << std::endl;
    double min = 0;
    if(C1.at<double>(0) < C2.at<double>(0)){
        min = C1.at<double>(0);
    }
    else{
        min = C2.at<double>(0);
    }

    std::cout << "\nCamera 1: Position -> \t(" << C1.at<double>(0) << ", " << C1.at<double>(1) << ", " << C1.at<double>(2)<< ")\t(units in cm)" << std::endl;
    std::cout << "Camera 2: Position -> \t(" << C2.at<double>(0) << ", " << C2.at<double>(1) << ", " << C2.at<double>(2)<< ")\t\t(units in cm)" << std::endl;
    
    //Save all the above information to a folder to be used by the range extraction code.
    saveFiles(stereo_YML_file, K1, K2, R1, R2, T1, T2, R, T, C1, C2, Po1, Po2, board_width, board_height, square_size, use_images, left_img.cols, left_img.rows);
    saveFiles(stereo_TXT_file, K1, K2, R1, R2, T1, T2, R, T, C1, C2, Po1, Po2, board_width, board_height, square_size, use_images, left_img.cols, left_img.rows);
    
    //Calculate reprojection error
//    double errorL = computeReprojectionErrors(left_object_points, left_image_points, left_rotation, left_translation, K1, D1temp, use_images);
//    double errorR = computeReprojectionErrors(right_object_points, right_image_points, right_rotation, right_translation, K2, D1temp, use_images);
//    std::cout << "\nLeft Reprojection Error: " << errorL << std::endl;
//    std::cout << "Right Reprojection Error: " << errorR << std::endl;

//    cv::Mat P1t, K1t, T1t, R1t;
//    ProjectionMat(left_image_points, left_object_points, P1t, K1t, T1t, R1t, num_corners, 0);
}
