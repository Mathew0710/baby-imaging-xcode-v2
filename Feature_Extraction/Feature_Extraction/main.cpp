//
//  main.cpp
//  Feature_Extraction
//
//  Created by Mathew Strydom on 2019/09/16.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <cminpack.h>
#include <stdio.h>
#include <iostream>
#include <cmath>


int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
//    Harris();
    lmder(<#cminpack_funcder_mn fcnder_mn#>, <#void *p#>, <#int m#>, <#int n#>, <#double *x#>, <#double *fvec#>, <#double *fjac#>, <#int ldfjac#>, <#double ftol#>, <#double xtol#>, <#double gtol#>, <#int maxfev#>, <#double *diag#>, <#int mode#>, <#double factor#>, <#int nprint#>, <#int *nfev#>, <#int *njev#>, <#int *ipvt#>, <#double *qtf#>, <#double *wa1#>, <#double *wa2#>, <#double *wa3#>, <#double *wa4#>);
    return 0;
}
