The Calibration source code can be found in the Calibration folder. Keep clicking on calibration until the cpp and include file are present.

In order to run the code you need OpenCV. This code is currently running on a Mac within the native IDE (Xcode). The calibration code has also been moved onto a begalebone black and is working perfectly.

The Range extraction code can be found in the Raneg extraction folder. Keep clicking on range_extraction until the cpp and include file are present. 

In order to change the image you wish to rectify and generate a disparity map for you need to go into the main.cpp file and change line 32 and 33 to the filename of the desired images.
Currently, the images used for calibration and range extraction can be found in the Pictures_V3 folder.

I have started implementing the feature extraction code in the form of a harris corner detector in the range extraction source code and therefore the feuture extraction folder is empty.

Thank you very much for looking at my work. I really appreciate it.
